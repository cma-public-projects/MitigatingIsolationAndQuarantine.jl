#=
This script demonstrates how to define a case isolation simulation, based on the
model described in: https://doi.org/10.1101/2021.12.23.21268326.

The outputs of this default script match the outputs from that paper.
=#

# MULTIPROCESSING ARGUMENTS ####################################################
using Distributed
addprocs(Threads.nthreads()-2)
# # addprocs(8)

# import required package
@everywhere using MitigatingIsolationAndQuarantine
################################################################################

using Distributions

# Define policies of interest ##################################################
# To view the fields of TestPolicy and allowed values use:
# dump(TestPolicy)
policies = [
            TestPolicy(NoTest(), 7, 7),
            TestPolicy(NoTest(), 10, 10),
            TestPolicy(NoTest(), 14, 14),
            TestPolicy(OneTest(), 7, 10),
            TestPolicy(OneTest(), 5, 10),
            TestPolicy(TwoTests(), 7, 10),
            TestPolicy(TwoTests(), 5, 10)
            ]

# Make any changes to default distribution values ##############################
#
# Note. they must use the identical key to the default (see defaultCasePars() in src/ModelDefaults.jl).
# You do not need to specify a distribution value if it is the same as the default one
#
# To define a distribution that has only one value (i.e. just 1 or 5 or 3.5 etc.),
# use `Normal(value, 0.0)`, which defines a delta function at the given 'value'.
# This is useful if you want to a fix a parameter at a given value for every realisation
# rather than it being drawn from a distribution.
model_pars = Dict{String, Any}()

# Define simulation parameters and outputs #####################################
num_sims        = 1000
pop_size        = 500000
output_folder   = "outputs"
simulation_name = "default_case_isolation"
save_results    = true
warn_about_file_overwrites = true
using_profiler  = false
saved_outputs_num_decimals = 4

# Run simulation ###############################################################
masterCaseSimulation(num_sims, pop_size,
    policies,
    model_pars,
    output_folder,
    simulation_name,
    save_results,
    warn_about_file_overwrites,
    using_profiler,
    saved_outputs_num_decimals)

# REMOVE WORKER PROCESSORS WHEN FINISHED #######################################
rmprocs(workers())
################################################################################
