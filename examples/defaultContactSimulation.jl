#=
This script demonstrates how to define a contact quarantine simulation, which is
inspired by the model described in: https://doi.org/10.1101/2021.12.23.21268326.
=#

# MULTIPROCESSING ARGUMENTS ####################################################
using Distributed
addprocs(Threads.nthreads()-2)
# # addprocs(8)

# import required package
@everywhere using MitigatingIsolationAndQuarantine
################################################################################

using Distributions, DataFrames

# Define policies of interest ##################################################
# To view the fields of TestPolicyContact and allowed values use:
# dump(TestPolicyContact)
policies = [
            TestPolicyContact(TestDailyYes(), QuarantineSymptoms(), [0,1,2,3,4,5,6,7], 8),
            TestPolicyContact(TestDailyYes(), QuarantineSymptoms(), [3,7], 8),
            TestPolicyContact(TestDailyYes(), QuarantineSymptoms(), [], 8),

            TestPolicyContact(TestDailyYes(), QuarantineIsoEndDays(), [0,1,2,3,4,5,6,7], 8),
            TestPolicyContact(TestDailyYes(), QuarantineIsoEndDays(), [3,7], 8),
            TestPolicyContact(TestDailyYes(), QuarantineIsoEndDays(), [], 8),

            TestPolicyContact(TestDailyNo(), QuarantineSymptoms(), [0,1,2,3,4,5,6,7], 8),
            TestPolicyContact(TestDailyNo(), QuarantineSymptoms(), [3,7], 8),
            TestPolicyContact(TestDailyNo(), QuarantineSymptoms(), [], 8),

            TestPolicyContact(TestDailyNo(), QuarantineIsoEndDays(), [0,1,2,3,4,5,6,7], 8),
            TestPolicyContact(TestDailyNo(), QuarantineIsoEndDays(), [3,7], 8),
            TestPolicyContact(TestDailyNo(), QuarantineIsoEndDays(), [], 8)
            ]

# Make any changes to default distribution values ##############################
#
# Note. they must use the identical key to the default (see defaultContactPars() in src/ModelDefaults.jl).
# You do not need to specify a distribution value if it is the same as the default one
#
# To define a distribution that has only one value (i.e. just 1 or 5 or 3.5 etc.),
# use `Normal(value, 0.0)`, which defines a delta function at the given 'value'.
# This is useful if you want to a fix a parameter at a given value for every realisation
# rather than it being drawn from a distribution.
model_pars = Dict{String, Any}(
                            "IPD_pars" => IPDPars(4.0, 0.1, 1.25, 0.1),
                            "test_sensitivity_dist" => Uniform(0.75, 0.85),
                            "test_sensitivity_asympt_dist" => Uniform(0.65, 0.75)
                            )

# Define simulation parameters and outputs #####################################
num_sims        = 1000
pop_size        = 500000
output_folder   = "outputs"
simulation_name = "default_contact_quarantine"
save_results    = true
warn_about_file_overwrites = true
using_profiler  = false
saved_outputs_num_decimals = 4

# Run simulation ###############################################################
masterContactSimulation(num_sims, pop_size,
    policies,
    model_pars,
    output_folder,
    simulation_name,
    save_results,
    warn_about_file_overwrites,
    using_profiler,
    saved_outputs_num_decimals)

# REMOVE WORKER PROCESSORS WHEN FINISHED #######################################
rmprocs(workers())
################################################################################
