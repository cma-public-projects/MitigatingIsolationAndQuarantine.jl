using Distributions, Random

struct NegativeSerialInterval <: ContinuousUnivariateDistribution
    α::Real
    θ::Real
    lower::Real
    upper::Real

    function NegativeSerialInterval(α::Real, θ::Real, lower::Real, upper::Real)
        new(α, θ, lower, upper)
    end
end

function Distributions.rand(rng::AbstractRNG, d::NegativeSerialInterval)::Float64
    -rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))
end

struct DifferenceOfSerialIntervals <: ContinuousUnivariateDistribution
    α::Real
    θ::Real
    lower::Real
    upper::Real

    function DifferenceOfSerialIntervals(α::Real, θ::Real, lower::Real, upper::Real) where {Real<:Real}
        new(α, θ, lower, upper)
    end
end

function Distributions.rand(rng::AbstractRNG, d::DifferenceOfSerialIntervals)::Float64

    interval_1 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))
    interval_2 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))

    return interval_1 - interval_2
end

struct AdditionOfSerialIntervals <: ContinuousUnivariateDistribution
    α::Real
    θ::Real
    lower::Real
    upper::Real

    function AdditionOfSerialIntervals(α::Real, θ::Real, lower::Real, upper::Real) where {Real<:Real}
        new(α, θ, lower, upper)
    end
end

function Distributions.rand(rng::AbstractRNG, d::AdditionOfSerialIntervals)::Float64

    interval_1 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))
    interval_2 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))

    return interval_1 + interval_2
end

struct TripleAdditionOfSerialIntervals <: ContinuousUnivariateDistribution
    α::Real
    θ::Real
    lower::Real
    upper::Real

    function TripleAdditionOfSerialIntervals(α::Real, θ::Real, lower::Real, upper::Real) where {Real<:Real}
        new(α, θ, lower, upper)
    end
end

function Distributions.rand(rng::AbstractRNG, d::TripleAdditionOfSerialIntervals)::Float64

    interval_1 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))
    interval_2 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))
    interval_3 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))

    return interval_1 + interval_2 + interval_3
end
