#=
This script demonstrates how to define a contact quarantine simulation, which is
inspired by the model described in: https://doi.org/10.1101/2021.12.23.21268326.
=#

# MULTIPROCESSING ARGUMENTS ####################################################
using Distributed
addprocs(Threads.nthreads()-2)
# # addprocs(8)

# import required package
@everywhere using MitigatingIsolationAndQuarantine

# import custom serial interval distributions for use in a MixtureModel for t_to_infectious_dist.
# file also in 'examples'
@everywhere include("serialIntervalDistributions.jl")
################################################################################

using Distributions, DataFrames

# Define policies of interest ##################################################
# To view the fields of TestPolicyContact and allowed values use:
# dump(TestPolicyContact)
policies = [
            TestPolicyContact(TestDailyYes(), QuarantineSymptoms(), [0,1,2,3,4,5,6,7], 8),
            TestPolicyContact(TestDailyYes(), QuarantineSymptoms(), [3,7], 8),
            TestPolicyContact(TestDailyYes(), QuarantineSymptoms(), [], 8),

            TestPolicyContact(TestDailyYes(), QuarantineIsoEndDays(), [0,1,2,3,4,5,6,7], 8),
            TestPolicyContact(TestDailyYes(), QuarantineIsoEndDays(), [3,7], 8),
            TestPolicyContact(TestDailyYes(), QuarantineIsoEndDays(), [], 8),

            TestPolicyContact(TestDailyNo(), QuarantineSymptoms(), [0,1,2,3,4,5,6,7], 8),
            TestPolicyContact(TestDailyNo(), QuarantineSymptoms(), [3,7], 8),
            TestPolicyContact(TestDailyNo(), QuarantineSymptoms(), [], 8),

            TestPolicyContact(TestDailyNo(), QuarantineIsoEndDays(), [0,1,2,3,4,5,6,7], 8),
            TestPolicyContact(TestDailyNo(), QuarantineIsoEndDays(), [3,7], 8),
            TestPolicyContact(TestDailyNo(), QuarantineIsoEndDays(), [], 8)
            ]

# TestPolicyContactExperimental policies are included for completeness, but not necessarily intended for use.
# May be useful to help the model explain data observed from a given policy.
# policies = [
#             TestPolicyContactExperimental(NoTest(), TestDailyYes(), QuarantineIsoEndDays(), collect(0:7), 
#                                             Dict(0=>0.5, 1=>0.5, 2=>0.5, 3=>1.0, 4=>0.5, 5=>0.5, 6=>0.5, 7=>1.0), 
#                                             8)
#             ]

# Make any changes to default distribution values ##############################
#
# Note. they must use the identical key to the default (see defaultContactPars() in src/ModelDefaults.jl).
# You do not need to specify a distribution value if it is the same as the default one
#
# To define a distribution that has only one value (i.e. just 1 or 5 or 3.5 etc.),
# use `Normal(value, 0.0)`, which defines a delta function at the given 'value'.
# This is useful if you want to a fix a parameter at a given value for every realisation
# rather than it being drawn from a distribution.

mixture_prior = [0.5, 0.05, 0.2, 0.05, 0.2]
α, θ = 1.35, 3.5

model_pars = Dict{String, Any}(
                            "IPD_pars" => IPDPars(2.62, 0.1, 1.88, 0.1),
                            "test_sensitivity_dist" => Uniform(0.75, 0.85),
                            "test_sensitivity_asympt_dist" => Uniform(0.65, 0.75),
                            "t_iso_entry_dist" => truncated(Normal(0.0, 1.0), -2.0, 2.0),
                            "t_to_pos_dist" => truncated(Weibull(2.0, 1.25), 0.0, 4.0),

                            "t_to_infectious_dist" => MixtureModel(
                                [truncated(Weibull(α, θ), lower=0, upper=14),
                                NegativeSerialInterval(α, θ, -14.0, 0.0),
                                DifferenceOfSerialIntervals(α, θ, -14.0, 14.0),
                                AdditionOfSerialIntervals(α, θ, 0.0, 21.0),
                                TripleAdditionOfSerialIntervals(α, θ, 0.0, 28.0)],
                                mixture_prior),

                            "t_quarantine_start_dict" => Dict{String, Any}(
                                "method" => StartFitData(), # can be StartAtZero(), StartFitData or StartProvideTestDelay
                                # observed distribution
                                "quarantine_start_df" => DataFrame("entry_days"=>collect(0:11),
                                                                    # prob will be automatically normalised to add up to 1.0
                                                                    "prob"=>[20.52, 34.33, 20.64, 10.12, 4.92, 2.77, 2.72, 2.51, 0.46, 0.31, 0.18, 0.51]
                                                                    ),
                                # "case_test_delay_dist_mean" => 0.8,
                                "rounding_func" => floor, # recommend floor or round
                                "show_fitted_distribution" => true,
                                "show_objectives" => true,
                                "save_plots" => true
                                )
                            )

# Define simulation parameters and outputs #####################################
num_sims        = 1000
pop_size        = 500000
output_folder   = "outputs"
simulation_name = "advanced_contact_quarantine"
save_results    = true
warn_about_file_overwrites = true
using_profiler  = false
saved_outputs_num_decimals = 4

# Run simulation ###############################################################
masterContactSimulation(num_sims, pop_size,
    policies,
    model_pars,
    output_folder,
    simulation_name,
    save_results,
    warn_about_file_overwrites,
    using_profiler,
    saved_outputs_num_decimals)

# REMOVE WORKER PROCESSORS WHEN FINISHED #######################################
rmprocs(workers())
################################################################################
