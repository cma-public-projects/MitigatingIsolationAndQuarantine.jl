# MitigatingIsolationAndQuarantine.jl

[![pipeline status](https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/badges/main/pipeline.svg)](https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/commits/main)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://cma-public-projects.gitlab.io/MitigatingIsolationAndQuarantine.jl/)


## Description

A Julia package for modelling COVID-19 isolation and quarantine in households. This package was developed by Covid-19 Modelling Aotearoa for the purposes of advising the Ministry of Health New Zealand and the New Zealand Government on the value of different policies for the isolation and quarantine of household cases and contacts, respectively.

The package is used to define two simulations: one that considers the household isolation of cases and the other that considers the quarantine of household contacts of cases that go onto become cases themselves.

## Citing

If you use this software as part of your research, teaching or other activities such as policy consideration please use the following citation:

```
@misc{,
  title={MitigatingIsolationAndQuarantine.jl--a package for considering COVID-19 home isolation and quarantine requirements in Julia},
  author={Trent, Joel and Harvey, Emily and Plank, Michael and O'Neale, Dion and Looker, Joshua},
  url={https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl}
  year={2022},
}
```

## Getting Started: Installation And First Steps

### Installing Julia

If you don't have a working installation of Julia we recommend installing it as an extension within Visual Studio Code using the steps linked [here](https://code.visualstudio.com/docs/languages/julia).

### Installing from Julia

To install the package, use the following command inside the Julia REPL:

```julia
using Pkg
Pkg.add(url="https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl")
```

To load the package, use the command:

```julia
using MitigatingIsolationAndQuarantine
```

### Updating to the most recent package version

If you want to make use of newer features added to the simulation in more recent versions than the one you have installed you can update the package using:

```julia
using Pkg
Pkg.update("MitigatingIsolationAndQuarantine")
```

To check the version installed use:

```julia
using Pkg
Pkg.status("MitigatingIsolationAndQuarantine")
```

## Authors and Acknowledgements

This package was scoped by Joel Trent, Emily Harvey, Michael Plank and Dion O'Neale, developed by Joel Trent, and received feedback on simulation implementation and user interface from Emily Harvey and Joshua Looker.

The simulation that considers the household isolation of cases was inspired by and built off of the work in this [paper](https://www.medrxiv.org/content/10.1101/2021.12.23.21268326v1) by Bays et al.

## Licensing

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

