using Documenter
using MitigatingIsolationAndQuarantine

makedocs(
    sitename = "MitigatingIsolationAndQuarantine.jl",
    format = Documenter.HTML(),
    modules = [MitigatingIsolationAndQuarantine],
    authors = "Joel Trent",
    repo = "https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/blob/{commit}{path}#{line}",
    pages = ["Home" => "index.md",
            "Simulation Details" => ["case_sim_details.md", "contact_sim_details.md"],
            "Tutorials" => ["case_tutorial.md", "contact_tutorial.md", "contact_tutorial_advanced.md"],
            "User Interface" => ["user_interface_shared.md", "user_interface_case.md", "user_interface_contact.md"],
            "Internal Library" => "library_internal.md"
            ]
)

# Documenter can also automatically deploy documentation to gh-pages.
# See "Hosting Documentation" and deploydocs() in the Documenter manual
# for more information.
# deploydocs(
#     repo = "gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl"
# )
