
```@index
Pages = ["user_interface_case.md"]
```

# Case Isolation User Interface

## Policy `structs`

```@docs
TestPolicy
NoTest
OneTest
TwoTests
```

## Default Model Parameters

```@docs
defaultCasePars
```

## Simulation function

```@docs
masterCaseSimulation
```
