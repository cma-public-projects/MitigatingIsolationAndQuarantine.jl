# Advanced Contact Quarantine Simulation Tutorial

In this tutorial we cover 'advanced' extensions that can be used within the contact quarantine model's parameter distributions to make the simulation more realistic.

As in [Contact Quarantine Simulation Tutorial](@ref) to define a contact quarantine simulation we:

1. Import package and set up distributed computing environment for parallel simulation.
2. Define a set of policies we are interested in simulating by creating an array of [`TestPolicyContact`](@ref) `structs`.
3. Make any desired changes to model parameter distributions (defaults will be used otherwise).
4. Name the simulation and where it is output, define the population size and number of realisations (and any other required arguments).
5. Run simulation.
6. Remove parallel workers.

Firstly, we will focus on two different extensions that can be used in Step 3, [More Realistic Infection Times of Contacts ](@ref) and [Non-Zero Quarantine Start Times](@ref). All other steps are the same as covered in [Contact Quarantine Simulation Tutorial](@ref).

Secondly, we will focus on an experimental extension to defining a policy in Step 2, [Randomly Choosing to Test on Testing Days for Model Comparison with Observed Data](@ref), by replacing [`TestPolicyContact`](@ref) `structs` with [`TestPolicyContactExperimental`](@ref) `structs`.

Files containing all the code used in this tutorial can be found in [advancedContactSimulation.jl](https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/-/blob/main/examples/advancedContactSimulation.jl) and [serialIntervalDistributions.jl](https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/-/blob/main/examples/serialIntervalDistributions.jl).

## Setup

```julia
# MULTIPROCESSING ARGUMENTS ####################################################
using Distributed
addprocs(Threads.nthreads()-2)
# # addprocs(8)

# import required package
@everywhere using MitigatingIsolationAndQuarantine

# import custom serial interval distributions for use in a MixtureModel for t_to_infectious_dist.
# file also in 'examples'
@everywhere include("serialIntervalDistributions.jl")
################################################################################

using Distributions, DataFrames
```

## More Realistic Infection Times of Contacts  

Up until now we have only demonstrated how to consider one chain of transmission for infected household contacts from the first detected case. Namely, we first determine when this case became infectious, ``t_\text{caseInfectious}`` (relative to their symptom onset at ``t=0``) and then add a random variable, ``x``, drawn from a serial interval distribution to determine when the contact becomes infectious, ``t_\text{contactInfectious} = t_\text{caseInfectious} + x``. This serial interval distribution is input into our `model_pars` dictionary using the key "t\_to\_infectious\_dist":

```julia
# example definition
model_pars = Dict{String, Any}(
                            "t_to_infectious_dist" => truncated(Weibull(1.35, 3.5), 0.0, 14.0)
                            )
```

This distribution represents the chain of transmission __X__``\rightarrow``__Y__, for household member __Y__, where __X__ is the first detected case in the household.

However, other chains of transmission may occur in reality such as:
- __Y__``\rightarrow``__X__
- __A__``\rightarrow``__X__ and __A__``\rightarrow``__Y__
- __X__``\rightarrow``__A__``\rightarrow``__Y__
- __X__``\rightarrow``__A__``\rightarrow``__B__``\rightarrow``__Y__

### Custom Distributions

To allow us to represent that any one of these chains of transmission might have occurred for household member __Y__ we need to be able to represent each of these chains of transmission with a custom defined distribution for "t\_to\_infectious\_dist" (see [serialIntervalDistributions.jl](https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/-/blob/main/examples/serialIntervalDistributions.jl)).

If we assume that the serial interval distribution is independent of the chain of transmission and is given by a truncated Weibull distribution (truncation is to prevent unlikely extremely large numbers from being drawn that impact output statistics) then we can define custom distributions which correspond to each chain of transmission.

!!! tip "Defining custom distributions"
    All custom distributions should be of type `ContinuousUnivariateDistribution` and also need to implement a method for `Distributions.rand`. If the custom distribution does not have this type then the simulation will throw a type error. Additional information can be found at [Create New Samplers and Distributions](https://juliastats.org/Distributions.jl/stable/extends/#Create-New-Samplers-and-Distributions).

For example, a negative serial interval distribution, corresponding to the __Y__``\rightarrow``__X__ chain of transmission can be defined using:

```julia
using Distributions, Random

struct NegativeSerialInterval <: ContinuousUnivariateDistribution
    α::Real
    θ::Real
    lower::Real
    upper::Real

    function NegativeSerialInterval(α::Real, θ::Real, lower::Real, upper::Real)
        new(α, θ, lower, upper)
    end
end

function Distributions.rand(rng::AbstractRNG, d::NegativeSerialInterval)::Float64
    -rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))
end
```

Similarly, a difference of serial intervals distribution, corresponding to the __A__``\rightarrow``__X__ and __A__``\rightarrow``__Y__ chain of transmission can be defined using:

```julia
using Distributions, Random

struct DifferenceOfSerialIntervals <: ContinuousUnivariateDistribution
    α::Real
    θ::Real
    lower::Real
    upper::Real

    function DifferenceOfSerialIntervals(α::Real, θ::Real, lower::Real, upper::Real) where {Real<:Real}
        new(α, θ, lower, upper)
    end
end

function Distributions.rand(rng::AbstractRNG, d::DifferenceOfSerialIntervals)::Float64

    interval_1 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))
    interval_2 = rand(truncated(Weibull(d.α, d.θ), d.lower, d.upper))

    return interval_1 - interval_2
end
```

Here, `interval_2` represents the serial interval between __A__ and __X__ and `interval_1` represents the serial interval between __A__ and __Y__.

### A MixtureModel of Custom Distributions

To allow us to represent that any one of these chains of transmission might have occurred for household member __Y__ we can use a `MixtureModel` from `Distributions.jl` with the custom defined distributions in [serialIntervalDistributions.jl](https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/-/blob/main/examples/serialIntervalDistributions.jl). This `MixtureModel` allows each individual contact in a simulation iteration to have one of these chains of transmission with a provided probability in `mixture_prior`. When "t\_to\_infectious\_dist" is a `MixtureModel` and `rand` is called on it, it will first choose a "component" from the vector of distributions with probability given by `mixture_prior` and secondly draw a random variable from that "component". An example of how to define this `MixtureModel` using these custom distributions, including `NegativeSerialInterval` and `DifferenceOfSerialIntervals` seen in [Custom Distributions](@ref) is seen below:

```julia
mixture_prior = [0.5, 0.05, 0.2, 0.05, 0.2]
α, θ = 1.35, 3.5

model_pars = Dict{String, Any}(
    "t_to_infectious_dist" => MixtureModel(
                                [truncated(Weibull(α, θ), lower=0, upper=14),
                                NegativeSerialInterval(α, θ, -14.0, 0.0),
                                DifferenceOfSerialIntervals(α, θ, -14.0, 14.0),
                                AdditionOfSerialIntervals(α, θ, 0.0, 21.0),
                                TripleAdditionOfSerialIntervals(α, θ, 0.0, 28.0)],
                                mixture_prior)
                        )
```

!!! warning "Use @everywhere when including the custom distributions when using a distributed environment"
    `@everywhere` needs to be appended when including custom distributions in a distributed environment (see [Setup](@ref)) so that each worker understands both the definition of the custom distribution and has access to the `Distributions.rand` method that is implemented. Otherwise, only the main worker will have access to these and an error will be thrown.

## Non-Zero Quarantine Start Times

Until now we have only considered the case were all contacts begin quarantine on exactly the same day as the first detected case's symptom onset (``t=0``). However, this is not realistic, either because the case cannot yet test positive on a RAT, symptoms are initially minor and not immediately checked or other possible reasons for later detection. Resultantly, we have implemented a reasonable method, if the model parameters used are a good reflection of reality and the data used is reliable, for determining how soon after symptom onset this first case is detected in integer days, which is assumed to be equivalent to the quarantine start time of their household contact. This method performs well against New Zealand level data from households in 2022. For discussion of the method used and it's drawbacks see [`MitigatingIsolationAndQuarantine.quarantineStartFit!`](@ref) and [`MitigatingIsolationAndQuarantine.testingDelayDF`](@ref).

### Default

The default case is when all quarantine start times occur at ``t=0``. Any other keys in `model_pars["t_quarantine_start_dict"]` will be ignored.

```julia
model_pars = Dict{String, Any}(
    "t_quarantine_start_dict" => Dict{String, Any}(
            "method" => StartAtZero()
            )
    )
```

### Fit Case Testing Delay

To fit an exponential case testing delay that makes the quarantine start distribution in the model approximate an observed quarantine start distribution we define the following dictionary. Note, the distributions defined by the user for keys "t\_iso\_entry\_dist" and "t\_to\_pos\_dist" (or the defaults if undefined) are used when fitting the testing delay.

```julia
model_pars = Dict{String, Any}(
    "t_iso_entry_dist" => truncated(Normal(0.0, 1.0), -2.0, 2.0),
    "t_to_pos_dist" => truncated(Weibull(2.0, 1.25), 0.0, 4.0),

    "t_quarantine_start_dict" => Dict{String, Any}(
        "method" => StartFitData(),
        # observed distribution
        "quarantine_start_df" => DataFrame("entry_days" => collect(0:11),
                                            # prob will be automatically normalised to add up to 1.0
                                            "prob" => [20.0, 25.0, 20.0, 15.00, 10.0, 5.0, 2.0, 1.0, 0.5, 0.5, 0.5, 0.5]
                                            ),
        "rounding_func" => floor, # recommend floor or round

        # optional arguments ###################################################
        "show_fitted_distribution" => true,
        "show_objectives" => true,
        "save_plots" => true
        )
    )
```

!!! tip "show_fitted_distribution"
    The user is not required to provide this key. If `show_fitted_distribution == true` the approximate quarantine start distribution given with the model's best exponential distribution for the case testing delay will be output against the distribution given in `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]` using [`MitigatingIsolationAndQuarantine.plotXvY`](@ref). Additionally, it will check if the user is happy with the fit before the simulation continues. If the key is not given this output will still be shown.

!!! tip "show_objectives"
    The user is not required to provide this key. If `show_objectives == true` the objective values found during the grid search used to fit the testing delay will be plotted.

!!! tip "save_plots"
    The user is not required to provide this key. If `save_plots == true` any plots shown during the fitting process will be saved in the same location as other model outputs.

### Providing A Case Testing Delay

To provide an exponential case testing delay we define the following dictionary. Note, the distributions defined by the user for keys "t\_iso\_entry\_dist" and "t\_to\_pos\_dist" (or the defaults if undefined) are used when outputting the comparison of the model's approximate quarantine start distribution and the provided observed distribution.

```julia
model_pars = Dict{String, Any}(
    "t_iso_entry_dist" => truncated(Normal(0.0, 1.0), -2.0, 2.0),
    "t_to_pos_dist" => truncated(Weibull(2.0, 1.25), 0.0, 4.0),

    "t_quarantine_start_dict" => Dict{String, Any}(
        "method" => StartProvideTestDelay(),
        "case_test_delay_dist_mean" => 0.8,
        "rounding_func" => floor, # recommend floor or round

        # optional arguments ###################################################
        "show_fitted_distribution" => true,
        # observed distribution
        "quarantine_start_df" => DataFrame("entry_days" => collect(0:11),
                                            # prob will be automatically normalised to add up to 1.0
                                            "prob" => [20.0, 25.0, 20.0, 15.00, 10.0, 5.0, 2.0, 1.0, 0.5, 0.5, 0.5, 0.5]
                                            ),
        "save_plots" => true
        )
    )
```

!!! tip "show_fitted_distribution"
    The user is not required to provide this key. If `show_fitted_distribution == true` the approximate quarantine start distribution given with the defined exponential distribution for the case testing delay will be output against the distribution given in `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]` using [`MitigatingIsolationAndQuarantine.plotXvY`](@ref). `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]` needs to exist for this to work properly. Additionally, it will check if the user is happy with the fit before the simulation continues. If the key is not given this output will not be shown.

!!! tip "save_plots"
    The user is not required to provide this key. If `save_plots == true` any plots shown during the fitting process will be saved in the same location as other model outputs.

!!! tip "Using StartProvideTestDelay instead of StartFitData"
    If the model is being run on a frequent basis (e.g. if called repeatedly to determine the most realistic model policy for a fixed set of parameters) or for consistency across different simulation files, and the distributions used for "t\_iso\_entry\_dist", "t\_to\_pos\_dist" and "quarantine\_start\_df" are fixed it would be best to run the model with [`StartFitData`](@ref) once to determine the best "case\_test\_delay\_dist\_mean" and then run the model with [`StartProvideTestDelay`](@ref) with this best "case\_test\_delay\_dist\_mean" for all subsequent simulations.

## Randomly Choosing to Test on Testing Days for Model Comparison with Observed Data

When comparing our expected model outputs on when infected quarantined household contacts are expected to be detected as cases to real world data we noticed that it was possible that many contacts may choose to test themselves irrespective of having symptoms or a day being a required testing day. Additionally, our model doesn't consider a policy where a potential subset of household contacts (e.g. essential workers) that may be allowed to go to work on a day if they return a negative test. This extension exists to gain insight into when contacts may chose to test themselves on days other than those required within a given policy. This extension is not intended to prove or disprove when individuals on average choose to test, as there may be other explanations for differences between the model and data, but it may be useful for gaining insight into potential testing patterns. Additionally, it is not intended for use when comparing policies from a theoretical standpoint. This is because when individuals choose to test themselves for different policies is likely to be influenced by their own and public perceptions of the policy, risk appetites and attitudes, the past and current national situation and other factors.

This extension is implemented relatively simply: If a policy does not require individuals who have symptoms at the start of the day to test, then, if day ``t = i`` is specified as a testing day with probability of choosing to test of ``p_\text{chooses\_to\_test}``, then each currently quarantined infected contact will randomly decide whether to get tested with probability ``p_\text{chooses\_to\_test}``. Alternatively, if a policy does require individuals who have symptoms at the start of the day to test, then, if day ``t = i`` is specified as a testing day with probability of choosing to test of ``p_\text{chooses\_to\_test}``, currently quarantined infected contacts with symptoms at day ``t = i`` will be tested and all others will randomly decide whether to get tested with probability ``p_\text{chooses\_to\_test}``.

We specify this extension using the [`TestPolicyContactExperimental`](@ref) struct. Note, you can not run a simulation with both [`TestPolicyContact`](@ref) and [`TestPolicyContactExperimental`](@ref) structs at the same time. The fourth field is now a dictionary, where for each integer day specified as a testing day in the third field, a probability of choosing to test (as a real number ``\in [0,1]``) must be provided, with the key as a corresponding integer.

!!! tip "TestPolicyContactExperimental fields"
    To view the fields of [`TestPolicyContactExperimental`](@ref) and allowed values you can use:

    ```@setup dumpContact
    using MitigatingIsolationAndQuarantine
    ```
    ```@repl dumpContact
    dump(TestPolicyContact)
    ```

For example, to specify a policy where currently quarantined infected contacts (who aren't currently symptomatic on a given day) test with probability 1.0 on days 3 and 7, and with probability 0.5 on days 0-2, and 4-6 we would do the following.

```julia
policies = [
            TestPolicyContactExperimental(TestDailyYes(), 
                                            QuarantineIsoEndDays(), collect(0:7), 
                                            Dict(0=>0.5, 1=>0.5, 2=>0.5, 3=>1.0, 
                                                    4=>0.5, 5=>0.5, 6=>0.5, 7=>1.0), 
                                            8)
            ]
```