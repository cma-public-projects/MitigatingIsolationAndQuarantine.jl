
```@index
Pages = ["user_interface_contact.md"]
```

# Contact Quarantine User Interface

## Policy `structs`

```@docs
TestPolicyContact
TestPolicyContactExperimental
TestDailyYes
TestDailyNo

QuarantineIsoEndDays
QuarantineSymptoms
NoQuarantine
```

## Quarantine start `structs`

```@docs
StartAtZero
StartFitData
StartProvideTestDelay
```

## Default Model Parameters

```@docs
defaultContactPars
```

## Simulation function

```@docs
masterContactSimulation
```
