# Case Isolation Simulation Tutorial

To define a case isolation simulation we:

1. Import package and set up distributed computing environment for parallel simulation.
2. Define a set of policies we are interested in simulating by creating an array of [`TestPolicy`](@ref) `structs`.
3. Make any desired changes to model parameter distributions (defaults will be used otherwise).
4. Name the simulation and where it is output, define the population size and number of realisations (and any other required arguments).
5. Run simulation.
6. Remove parallel workers.

A file containing all the code used in this tutorial can be found in [defaultCaseSimulation.jl](https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/-/blob/main/examples/defaultCaseSimulation.jl).

For additional description of the user interface see: [Case Isolation User Interface](@ref) and [Case Isolation Simulation Description and Assumptions](@ref).

## Import Package and Set Up Distributed Environment

This package is designed to be run in a distributed computing environment (see [Distributed.jl](https://docs.julialang.org/en/v1/stdlib/Distributed/)), meaning that the load of a given number of realisations for a simulation is split across multiple processors (workers). Running simulations in parallel in this manner can greatly increase simulation performance.

!!! tip "Recommended number of workers"
    The number of workers to use is recommended to be one or two less than the number of local threads (typically double the number of cores on a given CPU) on your CPU. This is because 1 worker already exists as the master worker and for most efficient distributed computing we want each worker to work on a distinct CPU thread. Furthermore, if you wish to use your computer at the same time as a simulation is running, we recommend that at least two threads are left available for other tasks.

We first allocate a number of workers for our simulation.

```julia
using Distributed
addprocs(Threads.nthreads()-2) # the number of parallel workers to use - on the author's system Threads.nthreads() returns 10
# addprocs(8) # add 8 workers - total number of workers will be 9

```

We can now check how many workers are allocated.

```julia
nprocs() # use to check the total number of worker processes allocated
workers() # the ids of the allocated workers
# Threads.nthreads() # the number of execution threads allocated to Julia at startup
```

We import our package with the annotation `@everywhere`, letting Julia know that we wish to load the package on all allocated workers.

!!! warning "Use of `@everywhere`"
    If `@everywhere` is not used at the beginning of the package import, then the package `MitigatingIsolationAndQuarantine` is only loaded onto the master worker's environment and cannot be seen by the other workers we wish to parallelise the simulation on. Resultantly, Julia will throw an error when we run [`masterCaseSimulation`](@ref). However, an error won't be thrown if we haven't added any worker processes (if `nprocs()==1`) using the above multiprocessing arguments (this package can run on a single thread).

```julia
@everywhere using MitigatingIsolationAndQuarantine
```

We also import the Distributions package which allows us to define any non-default parameter values via it's provided distributions.

```julia
using Distributions
```

## Define A Set Of Policies

We next define an array of policies using the [`TestPolicy`](@ref) struct. In a given [`TestPolicy`](@ref) we define the number of negative tests on consecutive days required to release a case early from isolation, the earliest day of release and the day their isolation ends regardless of symptoms or tests.

!!! tip "TestPolicy fields"
    To view the fields of [`TestPolicy`](@ref) and allowed values you can use:
    ```@setup dumpCase
    using MitigatingIsolationAndQuarantine
    ```
    ```@repl dumpCase
    dump(TestPolicy)
    ```

```julia
policies = [
            TestPolicy(NoTest(), 7, 7),
            TestPolicy(NoTest(), 10, 10),
            TestPolicy(NoTest(), 14, 14),
            TestPolicy(OneTest(), 7, 10),
            TestPolicy(OneTest(), 5, 10),
            TestPolicy(TwoTests(), 7, 10),
            TestPolicy(TwoTests(), 5, 10)
            ]
```

##  Make Desired Changes To Model Parameter Distributions

We then make any changes to model parameter distributions by defining a dictionary. If we want to only use defaults then we define an empty dictionary.

!!! tip "Default distribution values"
    A distribution value for a parameter does not need to be supplied if it is the same as the default one returned by [`defaultCasePars`](@ref).

!!! tip "Distribution that has one value"
    To define a distribution that has only one value (i.e. just 1 or 5 or 3.5 etc.), use `Normal(value, 0.0)`, which defines a delta function at the given 'value'. This is useful if you want to a fix a parameter at a given value for every realisation rather than it being drawn from a distribution.

!!! compat "model_pars dictionary definition"
    The defined dictionary needs to be of the form shown below so that it is accepted as an input to [`masterCaseSimulation`](@ref). Additionally, any dictionary keys used need to be identical to the keys in the default parameter dictionary returned by [`defaultCasePars`](@ref).

!!! warning "Make sure to use truncated continuous distributions"
    All continuous distributions defined by the user that are bounded on at least one side by a magnitude of ``\infty`` should be truncated to prevent extremely large values, that can massively skew output statistics (which are generally means), from being drawn from these distributions. Even though this is rare, when simulations are performed 1000 times with a population size of 500,000 values are drawn ``5 \times 10^{8}`` times, ithout truncation this is relatively likely to occur at least once per simulation. An example of this is seen for `t_iso_entry_dist` below.

```julia
# model_pars = Dict{String, Any}() # empty dictionary - use defaults

# example definition (note: identical distributions to defaults)
model_pars = Dict{String, Any}(
                            "test_sensitivity_dist" => Uniform(0.7,0.8),
                            "t_to_neg_dist" => Normal(0.0, 0.0),
                            "t_iso_entry_dist" => truncated(Normal(0.0, 0.3), -3.0, 3.0)
                                )
```

## Name the simulation and additional parameters

We define the number of realisations of policy to simulate as well as the population size to simulate on (the number of cases modelled as in isolation). As mentioned in [`MitigatingIsolationAndQuarantine.writeMetrics!`](@ref), the output folder is a valid path to a potential directory (either defined from the current working directory or as an absolute path). The directory does not have to exist - if it does not, the path to that directory and the directory itself will be created.

See [`masterCaseSimulation`](@ref) for more information on these input parameters.

```julia
num_sims        = 1000
pop_size        = 500000
output_folder   = "outputs"
simulation_name = "default_case_isolation"
save_results    = true
warn_about_file_overwrites = true
using_profiler  = false
```

## Run simulation

Finally, we run the simulation.

!!! note "Warning about files being overwritten"
    If we are saving results and have set `warn_about_file_overwrites == true` then the simulation will warn the user if any files exist that would be overwritten by outputs generated by the simulation. It will ask if the user is ok with those being overwritten: if yes, then the simulation will continue, otherwise the simulation will be terminated.

```julia
masterCaseSimulation(num_sims, pop_size,
    policies,
    model_pars,
    output_folder,
    simulation_name,
    save_results,
    warn_about_file_overwrites,
    using_profiler)
```

## Remove parallel workers

As a final step when the simulation is finished it is recommended that all parallel workers are removed.

```julia
rmprocs(workers())
```
