# MitigatingIsolationAndQuarantine.jl: Modelling of COVID-19 Isolation and Quarantine of Households

This package was developed by [Covid-19 Modelling Aotearoa](https://www.covid19modelling.ac.nz/) for the purposes of advising the Ministry of Health New Zealand and the New Zealand Government on the value of different policies for the isolation and quarantine of household cases and contacts, respectively.

The package is used to define two simulations: one that considers the household isolation of cases and the other that considers the quarantine of household contacts of cases that go onto become cases themselves.

Tutorials on how to setup and run these simulations can be found in [Case Isolation Simulation Tutorial](@ref), [Contact Quarantine Simulation Tutorial](@ref) and  [Advanced Contact Quarantine Simulation Tutorial](@ref). Corresponding files for these tutorials can be found in [examples](https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl/-/tree/main/examples).

A deeper dive into the user interface can be found in [Case Isolation User Interface](@ref) and [Contact Quarantine User Interface](@ref).

Full descriptions of the underlying simulations being run, including their assumptions and description of outputs can be found in [Case Isolation Simulation Description and Assumptions](@ref) and [Contact Quarantine Simulation Description and Assumptions](@ref).

Full documentation of the internal library can be found in [Internal Library](@ref).

## Citing

If you use this software as part of your research, teaching or other activities such as policy consideration please use the following citation:

```
@misc{,
  title={MitigatingIsolationAndQuarantine.jl--a package for considering COVID-19 home isolation and quarantine requirements in Julia},
  author={Trent, Joel and Harvey, Emily and Plank, Michael and O'Neale, Dion and Looker, Joshua},
  url={https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl}
  year={2022},
}
```

## Getting Started: Installation and First Steps

### Installing Julia

If you don't have a working installation of Julia we recommend installing it as an extension within Visual Studio Code using the steps linked [here](https://code.visualstudio.com/docs/languages/julia).

### Installing from Julia

To install the package, use the following command inside the Julia REPL:

```julia
using Pkg
Pkg.add(url="https://gitlab.com/cma-public-projects/MitigatingIsolationAndQuarantine.jl")
```

To load the package, use the command:

```julia
using MitigatingIsolationAndQuarantine
```

### Updating to the most recent package version

If you want to make use of newer features added to the simulation in more recent versions than the one you have installed you can update the package using:

```julia
using Pkg
Pkg.update("MitigatingIsolationAndQuarantine")
```

To check the version installed use:

```julia
using Pkg
Pkg.status("MitigatingIsolationAndQuarantine")
```

## Authors and Acknowledgements

This package was scoped by Joel Trent, Emily Harvey, Michael Plank and Dion O'Neale, developed by Joel Trent, and received feedback on simulation implementation and user interface from Emily Harvey and Joshua Looker.

The simulation that considers the household isolation of cases was inspired by and built off of the work in this [paper](https://www.medrxiv.org/content/10.1101/2021.12.23.21268326v1) by Bays et al.
