
```@index
Pages = ["library_internal.md"]
```

# Internal Library

Documentation for `structs` and functions not covered within user interface documentation ([Case Isolation User Interface](@ref), [Contact Quarantine User Interface](@ref) and [Shared Interface](@ref)) or simulation description and assumptions ([Case Isolation Simulation Description and Assumptions](@ref) and [Contact Quarantine Simulation Description and Assumptions](@ref)).

## Model `structs`

```@docs
MitigatingIsolationAndQuarantine.IsolationModel
MitigatingIsolationAndQuarantine.QuarantineModel
MitigatingIsolationAndQuarantine.QuarantineModelExperimental
```

```@docs
MitigatingIsolationAndQuarantine.CaseDistributions
MitigatingIsolationAndQuarantine.ContactDistributions
```

```@docs
MitigatingIsolationAndQuarantine.CaseSimulation
MitigatingIsolationAndQuarantine.ContactSimulation
```

## Contact Quarantine Start Fitting Functions

```@docs
MitigatingIsolationAndQuarantine.quarantineStartFit!
MitigatingIsolationAndQuarantine.sortedConvolution
MitigatingIsolationAndQuarantine.fitTestingDelayDist!
MitigatingIsolationAndQuarantine.testingDelayDF
MitigatingIsolationAndQuarantine.testingDelayObj
MitigatingIsolationAndQuarantine.countmapDF
MitigatingIsolationAndQuarantine.plotXvY
MitigatingIsolationAndQuarantine.plotXvLogY
```

## Initialisation Functions

```@docs
MitigatingIsolationAndQuarantine.createModelStruct
MitigatingIsolationAndQuarantine.numTestsToInt
MitigatingIsolationAndQuarantine.calcMaxIso
MitigatingIsolationAndQuarantine.initModelCase
MitigatingIsolationAndQuarantine.initModelContact
MitigatingIsolationAndQuarantine.initPopDataframe
MitigatingIsolationAndQuarantine.initPopDataframeContact
MitigatingIsolationAndQuarantine.initTestSensitivity
MitigatingIsolationAndQuarantine.initMetricDataframe
MitigatingIsolationAndQuarantine.initMetricDataframeContact
MitigatingIsolationAndQuarantine.initPolicyDataframe
MitigatingIsolationAndQuarantine.initPolicyDataframeContact
MitigatingIsolationAndQuarantine.initGlobMetricDataframe
MitigatingIsolationAndQuarantine.initGlobMetricDataframeContact
MitigatingIsolationAndQuarantine.initPropDailyDataframe
MitigatingIsolationAndQuarantine.initPropDailyDetectDataframe
```

## Simulation Functions

```@docs
MitigatingIsolationAndQuarantine.casePolicySimulation
MitigatingIsolationAndQuarantine.contactPolicySimulation
```

## Output Functions

```@docs
MitigatingIsolationAndQuarantine.printTimeElapsed
MitigatingIsolationAndQuarantine.extractPropDailyMetrics
MitigatingIsolationAndQuarantine.computeGlobMetrics!
MitigatingIsolationAndQuarantine.writeMetrics!
MitigatingIsolationAndQuarantine.warnOfMetricOverwrites
```
