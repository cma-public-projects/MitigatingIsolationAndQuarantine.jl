module MitigatingIsolationAndQuarantine

using Distributed, SharedArrays
using DataFrames, DataFramesMeta, Random, Distributions, Configurations
using CSV, Statistics, Dates, StatsBase
using PyPlot, Seaborn
using Pkg

using ProgressMeter
global const PROGRESS__METER__DT = 0.2

export masterCaseSimulation, masterContactSimulation, # simulation functions
    defaultCasePars, defaultContactPars, # default parameter output functions
    # model structs
    TestPolicy, TestPolicyContact, TestPolicyContactExperimental, 
    NoTest, OneTest, TwoTests, TestDailyYes, TestDailyNo,
    QuarantineSymptoms, QuarantineIsoEndDays, NoQuarantine, IPDPars,
    StartAtZero, StartFitData, StartProvideTestDelay

include("model_structs.jl")
include("initialisation_functions.jl")
include("model_defaults.jl")
include("output_functions.jl")
include("contact_quarantine_start_fit.jl")
include("simulation_functions.jl")

end # module
