#=
A file containing all structs used to define the isolation and quarantine models

Author: Joel Trent
=#
"""
    CaseSimulation()

Specifies that the current simulation is for case isolation.
"""
struct CaseSimulation; end

"""
    ContactSimulation()

Specifies that the current simulation is for contact quarantine.
"""
struct ContactSimulation; end

# Case Isolation structs #######################################################
"""
    NoTest()

Specifies that no consecutive daily tests will be used during case isolation. Unlike the other structs which specify the number of consecutive daily tests to be released early, [`OneTest`](@ref) and [`TwoTests`](@ref), specifying [`NoTest`](@ref) will mean that cases cannot be released early from isolation. This creates the simplest case isolation policy, where the case has a mandatory isolation period and cannot be released early, irrespective of any test results.
"""
struct NoTest; end

"""
    OneTest()

Specifies that one negative test is required to release a case early from isolation.

Daily testing of this case will occur from the early release day until a negative test is returned and they are released or the isolation end day is reached (and they are then released).
"""
struct OneTest; end

"""
    TwoTests()

Specifies that two negative tests, performed on consecutive days, are required to release a case early from isolation.

Daily testing of this case will occur from the day before the early release day until two consecutive negative tests are returned and they are released or the isolation end day is reached (and they are then released).
"""
struct TwoTests; end

"""
    TestPolicy(num_tests::Union{NoTest, OneTest, TwoTests},
        t_early_release::Int64,
        t_iso_end::Int64)

Defines a case isolation testing policy.

# Arguments
- `num_tests::Union{NoTest, OneTest, TwoTests}`: If of type, [`OneTest()`](@ref) and [`TwoTests()`](@ref), then it is number of negative tests on consecutive days required to release a case early from isolation. For type [`NoTest()`](@ref) no tests are used in case isolation and cases cannot be released early (`t_early_release` has no affect).
- `t_early_release::Int64`: The earliest day a case can be released from isolation if they return the given number of negative tests on consecutive days. *Note*, `t=0` is treated as the day cases enter isolation, so `t_early_release` must be at least day 1.
- `t_iso_end`: The day a case will be released from isolation regardless of health status. *Note*, `t=0` is treated as the day cases enter isolation, so `t_iso_end` must be at least day 1.
"""
struct TestPolicy
    num_tests::Union{NoTest, OneTest, TwoTests}
    t_early_release::Int64
    t_iso_end::Int64
end
################################################################################

# Contact Quarantine structs ###################################################
"""
    TestDailyYes()

Specifies that if a quarantined contact is symptomatic on a given day, they will be tested.
"""
struct TestDailyYes; end

"""
    TestDailyNo()

Specifies that if a quarantined contact is symptomatic on a given day, they will not be tested.
"""
struct TestDailyNo; end

"""
    QuarantineIsoEndDays()

Specifies that quarantined contacts are required to quarantine for `t_iso_end` days before they are allowed into the community again, regardless of testing negative or symptoms. Additionally, after `t_iso_end` days they are required to quarantine if symptomatic at the start of a day. Otherwise, they are allowed into the community. If at any point they test positive they become a case and are not allowed out into the community for the remainder of the simulation.
"""
struct QuarantineIsoEndDays; end

"""
    QuarantineSymptoms()

Specifies that quarantined contacts are required to quarantine if symptomatic at the start of a day, but are allowed into the community otherwise. If at any point they test positive they become a case and are not allowed out into the community for the remainder of the simulation.
"""
struct QuarantineSymptoms;end

"""
    NoQuarantine()

Specifies that quarantined contacts are not required to quarantine and are allowed into the community. If at any point they test positive they become a case and are not allowed out into the community for the remainder of the simulation.
"""
struct NoQuarantine; end

"""
    TestPolicyContact(symptomatic_test_policy::Union{TestDailyYes, TestDailyNo},
        quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays},
        t_test_days::Array{Int64, 1},
        t_iso_end::Int64)

Defines a contact quarantine testing policy. If a quarantined contact tests positive on any day they are then considered to be a case and would undergo the isolation procedures simulated using [`masterCaseSimulation`](@ref). Resultantly, any contribution they make to output metrics is frozen at that day.

# Arguments
- `symptomatic_test_policy::Union{TestDailyYes, TestDailyNo}`: Whether or not a quarantined contact is tested when symptomatic on a given day.
- `quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays}`: The strictness of the quarantine policy used, defining whether or not individuals are allowed into the community during their quarantine period. For more see [`NoQuarantine()`](@ref), [`QuarantineSymptoms()`](@ref) and [`QuarantineIsoEndDays()`](@ref).
- `t_test_days::Array{Int64, 1}`: Days on which all quarantined contacts that have not yet tested positive are tested irrespective of `symptomatic_test_policy`. On days where `symptomatic_test_policy` also requires a test to be done, only one test is performed.
- `t_iso_end`: The day a case will be released from isolation regardless of health status. *Note*, `t=0` is treated as the day they enter isolation.
"""
struct TestPolicyContact
    symptomatic_test_policy::Union{TestDailyYes, TestDailyNo}
    quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays}
    t_test_days::Array{Int64, 1}
    t_iso_end::Int64
end

"""
    TestPolicyContactExperimental(symptomatic_test_policy::Union{TestDailyYes, TestDailyNo},
        quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays},
        t_test_days::Array{Int64, 1},
        prob_testing_on_test_days::Dict{Int64, Real},
        t_iso_end::Int64)

Defines a experimental contact quarantine testing policy where contacts choose to test on specified test days with some probability. If a quarantined contact tests positive on any day they are then considered to be a case and would undergo the isolation procedures simulated using [`masterCaseSimulation`](@ref). Resultantly, any contribution they make to output metrics is frozen at that day. Not intended for use in the theoretical comparison of policies. It may be useful to help the model explain data observed from a given policy.

# Arguments
- `symptomatic_test_policy::Union{TestDailyYes, TestDailyNo}`: Whether or not a quarantined contact is tested when symptomatic on a given day.
- `quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays}`: The strictness of the quarantine policy used, defining whether or not individuals are allowed into the community during their quarantine period. For more see [`NoQuarantine()`](@ref), [`QuarantineSymptoms()`](@ref) and [`QuarantineIsoEndDays()`](@ref).
- `t_test_days::Array{Int64, 1}`: Days on which all quarantined contacts that have not yet tested positive are tested with probability `prob_testing_on_test_days[test_day]` irrespective of `symptomatic_test_policy`. On days where `symptomatic_test_policy` also requires a test to be done, only one test is performed. I.e. if day i is a specified as a test day, but they do not choose to test after drawing whether they will test with probability `prob_testing_on_test_days[i]`, then if the `symptomatic_test_policy` also requires a test to be done, they will be tested due to symptoms. 
- `prob_testing_on_test_days::Dict{Int64, Real}`: On day `i`, if day `i` is in `t_test_days`, prob_testing_on_test_days[i] is the probability of a quarantined contact choosing to test. Because of this we require `sum([haskey(prob_testing_on_test_days, i) for i in t_test_days]) == length(t_test_days)`, otherwise the simulation will break - every test day must be a valid dictionary key.
- `t_iso_end`: The day a case will be released from isolation regardless of health status. *Note*, `t=0` is treated as the day they enter isolation.
"""
struct TestPolicyContactExperimental
    symptomatic_test_policy::Union{TestDailyYes, TestDailyNo}
    quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays}
    t_test_days::Array{Int64, 1}
    prob_testing_on_test_days::Dict{Int64, Real}
    t_iso_end::Int64
end

"""
    StartAtZero()

Specifies that the quarantine start time for all contacts will be set to ``t=0``.
"""
struct StartAtZero; end

"""
    StartFitData()

Specifies that the quarantine start time for contacts within the model will be fitted to empirical data, given the user-provided "t\\_iso\\_entry\\_dist" and "t\\_to\\_infectious\\_dist" distributions. The fitting process is described in [`MitigatingIsolationAndQuarantine.fitTestingDelayDist!`](@ref), where an exponential case testing delay distribution is fit, and the approximation of the empirical quarantine start time distribution is described in [`MitigatingIsolationAndQuarantine.testingDelayDF`](@ref). Requires the user to provide a dataframe of empirical quarantine start time data across integer "entry\\_days" with proportion on each day, "prob", as well as a valid rounding function (rounds a floating point number to an integer value).
"""
struct StartFitData; end


"""
    StartProvideTestDelay()

Specifies that the quarantine start time for contacts within the model will be produced using the approximation described in [`MitigatingIsolationAndQuarantine.testingDelayDF`](@ref). Requires the user to provide a mean value for the exponential case testing delay distribution as well as a valid rounding function (rounds a floating point number to an integer value).
"""
struct StartProvideTestDelay; end
################################################################################

"""
    IPDPars(IPD_shape_mean::Number,
            IPD_shape_sd::Number,
            IPD_scale_mean::Number,
            IPD_scale_sd::Number)

Defines the shape and scale of the Gamma distribution used to model the infectious period distribution of cases. A number generated from this distribution is the length of time an individual is infectious for. The shape and scale are drawn for each realisation of a policy simulation from a normal distribution with the provided mean and standard deviation. These are then used to draw the infectious period for each individual separately from a Gamma distribution.

# Arguments
- `IPD_shape_mean::Number`: The mean used by a Normal distribution for the shape parameter.
- `IPD_shape_sd::Number`: The standard deviation used by a Normal distribution for the shape parameter.
- `IPD_scale_mean::Number`: The mean used by a Normal distribution for the scale parameter.
- `IPD_scale_sd::Number`: The standard deviation used by a Normal distribution for the scale parameter.

## Example use for 4 cases
```@repl
julia> using MitigatingIsolationAndQuarantine
julia> using Distributions

julia> IPD = IPDPars(4, 0.1, 1.25, 0.1)
IPDPars(4, 0.1, 1.25, 0.1)

julia> pop_size = 4
4

julia> infectious_period =
                rand.(Gamma(
                      rand(truncated(Normal(IPD.IPD_shape_mean, IPD.IPD_shape_sd), 0, 100)),
                      rand(truncated(Normal(IPD.IPD_scale_mean, IPD.IPD_scale_sd), 0, 100))
                      ), pop_size)
4-element Vector{Float64}:
 3.615939318250912
 3.5410546495954573
 1.6753994704970594
 7.072967908328591
```
"""
struct IPDPars
    IPD_shape_mean::Number
    IPD_shape_sd::Number
    IPD_scale_mean::Number
    IPD_scale_sd::Number
end

"""
    IsolationModel(t_init::Int64,
        t_early_release::Int64,
        t_iso_end::Int64,
        test_policy::Union{NoTest, OneTest, TwoTests},
        pop_size::Int64,
        IPD_shape_mean::Number,
        IPD_shape_sd::Number,
        IPD_scale_mean::Number
        IPD_scale_sd::Number,
        test_sensitivity_dist::ContinuousUnivariateDistribution,
        t_iso_entry_dist::ContinuousUnivariateDistribution,
        t_to_pos_dist::ContinuousUnivariateDistribution,
        t_to_neg_dist::ContinuousUnivariateDistribution)

Internally used `struct` containing all policy and parameter information required to define each individual realisation of a singular policy within a case isolation simulation.

Arguments are given by the arguments from [`TestPolicy`](@ref) (Note: `IsolationModel.test_policy == TestPolicy.num_tests`) and [`MitigatingIsolationAndQuarantine.CaseDistributions`](@ref) as well as `pop_size` which specifies the size of the simulated population to use.
"""
struct IsolationModel

    t_init::Int64
    t_early_release::Int64
    t_iso_end::Int64
    test_policy::Union{NoTest, OneTest, TwoTests}

    pop_size::Int64

    IPD_shape_mean::Number
    IPD_shape_sd::Number

    IPD_scale_mean::Number
    IPD_scale_sd::Number

    test_sensitivity_dist::ContinuousUnivariateDistribution

    t_iso_entry_dist::ContinuousUnivariateDistribution

    # time to testing positive after infectious
    t_to_pos_dist::ContinuousUnivariateDistribution

    # time to testing negative once no longer infectious
    t_to_neg_dist::ContinuousUnivariateDistribution
end

"""
    QuarantineModel(t_init::Int64,
        t_iso_end::Int64,
        symptomatic_test_policy::Union{TestDailyYes, TestDailyNo},
        quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays},
        t_test_days::Array{Int64, 1},
        pop_size::Int64,
        IPD_shape_mean::Number,
        IPD_shape_sd::Number,
        IPD_scale_mean::Number,
        IPD_scale_sd::Number,
        test_sensitivity_dist::ContinuousUnivariateDistribution,
        test_sensitivity_asympt_dist::ContinuousUnivariateDistribution,
        t_iso_entry_dist::ContinuousUnivariateDistribution,
        t_to_infectious_dist::ContinuousUnivariateDistribution
        t_to_pos_dist::ContinuousUnivariateDistribution,
        t_to_neg_dist::ContinuousUnivariateDistribution,
        t_sympt_start_dist::ContinuousUnivariateDistribution,
        t_sympt_end_dist::ContinuousUnivariateDistribution,
        prop_asympt_dist::ContinuousUnivariateDistribution,
        t_quarantine_start_dict::Dict)

Internally used `struct` containing all policy and parameter information required to define each individual realisation of a singular policy within a contact quarantine simulation.

Arguments are given by the arguments from [`TestPolicyContact`](@ref) (Note: `QuarantineModel.test_policy == TestPolicyContact.num_tests`) and [`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref) as well as `pop_size` which specifies the size of the simulated population to use.
"""
struct QuarantineModel

    t_init::Int64
    t_iso_end::Int64
    symptomatic_test_policy::Union{TestDailyYes, TestDailyNo}
    quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays}
    t_test_days::Array{Int64, 1}

    pop_size::Int64

    IPD_shape_mean::Number
    IPD_shape_sd::Number

    IPD_scale_mean::Number
    IPD_scale_sd::Number

    test_sensitivity_dist::ContinuousUnivariateDistribution

    test_sensitivity_asympt_dist::ContinuousUnivariateDistribution

    t_iso_entry_dist::ContinuousUnivariateDistribution

    # serial interval
    t_to_infectious_dist::ContinuousUnivariateDistribution

    # time to testing positive after infectious
    t_to_pos_dist::ContinuousUnivariateDistribution

    # time to testing negative once no longer infectious
    t_to_neg_dist::ContinuousUnivariateDistribution

    t_sympt_start_dist::ContinuousUnivariateDistribution

    t_sympt_end_dist::ContinuousUnivariateDistribution

    prop_asympt_dist::ContinuousUnivariateDistribution

    # time contact enters quarantine relative to t=0
    t_quarantine_start_dict::Dict
end

"""
    QuarantineModelExperimental(t_init::Int64,
        t_iso_end::Int64,
        symptomatic_test_policy::Union{TestDailyYes, TestDailyNo},
        quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays},
        t_test_days::Array{Int64, 1},
        prob_testing_on_test_days::Array{Number, 1},
        pop_size::Int64,
        IPD_shape_mean::Number,
        IPD_shape_sd::Number,
        IPD_scale_mean::Number,
        IPD_scale_sd::Number,
        test_sensitivity_dist::ContinuousUnivariateDistribution,
        test_sensitivity_asympt_dist::ContinuousUnivariateDistribution,
        t_iso_entry_dist::ContinuousUnivariateDistribution,
        t_to_infectious_dist::ContinuousUnivariateDistribution
        t_to_pos_dist::ContinuousUnivariateDistribution,
        t_to_neg_dist::ContinuousUnivariateDistribution,
        t_sympt_start_dist::ContinuousUnivariateDistribution,
        t_sympt_end_dist::ContinuousUnivariateDistribution,
        prop_asympt_dist::ContinuousUnivariateDistribution,
        t_quarantine_start_dict::Dict)

Internally used `struct` containing all policy and parameter information required to define each individual realisation of a singular policy within a contact quarantine simulation.

Arguments are given by the arguments from [`TestPolicyContactExperimental`](@ref) (Note: `QuarantineModelExperimental.test_policy == TestPolicyContactExperimental.num_tests`) and [`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref) as well as `pop_size` which specifies the size of the simulated population to use.
"""
struct QuarantineModelExperimental

    t_init::Int64
    t_iso_end::Int64
    symptomatic_test_policy::Union{TestDailyYes, TestDailyNo}
    quarantine_policy::Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays}
    t_test_days::Array{Int64, 1}
    prob_testing_on_test_days::Dict{Int64, Real}

    pop_size::Int64

    IPD_shape_mean::Number
    IPD_shape_sd::Number

    IPD_scale_mean::Number
    IPD_scale_sd::Number

    test_sensitivity_dist::ContinuousUnivariateDistribution

    test_sensitivity_asympt_dist::ContinuousUnivariateDistribution

    t_iso_entry_dist::ContinuousUnivariateDistribution

    # serial interval
    t_to_infectious_dist::ContinuousUnivariateDistribution

    # time to testing positive after infectious
    t_to_pos_dist::ContinuousUnivariateDistribution

    # time to testing negative once no longer infectious
    t_to_neg_dist::ContinuousUnivariateDistribution

    t_sympt_start_dist::ContinuousUnivariateDistribution

    t_sympt_end_dist::ContinuousUnivariateDistribution

    prop_asympt_dist::ContinuousUnivariateDistribution

    # time contact enters quarantine relative to t=0
    t_quarantine_start_dict::Dict
end


"""
    CaseDistributions(IPD_pars::IPDPars,
        test_sensitivity_dist::ContinuousUnivariateDistribution,
        t_iso_entry_dist::ContinuousUnivariateDistribution,
        t_to_pos_dist::ContinuousUnivariateDistribution,
        t_to_neg_dist::ContinuousUnivariateDistribution)

A struct of model parameters for a case isolation simulation. For defaults see [`defaultCasePars`](@ref). Continuous distributions that are bounded on at least one side by a magnitude of ``\\infty`` should be truncated using [`truncated`](https://juliastats.org/Distributions.jl/stable/truncate/#Distributions.truncated) so that output metrics that are means are not accidentally skewed by rare large values.

# Arguments
- `IPD_pars::IPDPars`: Infectious period distribution, see [`IPDPars`](@ref).
- `test_sensitivity_dist::ContinuousUnivariateDistribution`: Distribution of individual's rapid antigen test (RAT) sensitivity - the probability that a RAT will return positive for a given case while they're infectious (offset by `t_to_pos_dist` and `t_to_neg_dist`).
- `t_iso_entry_dist::ContinuousUnivariateDistribution`: Distribution modelling an offset between the start of the isolation period for a given case (t=0) and the start of their infectious period.
- `t_to_pos_dist::ContinuousUnivariateDistribution`: Distribution modelling the time it takes for an individual to be able to test positive on a RAT after the start of their infectious period (defaulted to zero).
- `t_to_neg_dist::ContinuousUnivariateDistribution`: Distribution modelling the time it takes for an individual to no longer test positive on a RAT after the end of their infectious period.
"""
@option struct CaseDistributions

    IPD_pars::IPDPars
    test_sensitivity_dist::ContinuousUnivariateDistribution
    t_iso_entry_dist::ContinuousUnivariateDistribution
    # time to testing positive after infectious
    t_to_pos_dist::ContinuousUnivariateDistribution
    # time to testing negative once no longer infectious
    t_to_neg_dist::ContinuousUnivariateDistribution
end

"""
    ContactDistributions(IPD_pars::IPDPars,
        test_sensitivity_dist::ContinuousUnivariateDistribution,
        test_sensitivity_asympt_dist::ContinuousUnivariateDistribution,
        t_iso_entry_dist::ContinuousUnivariateDistribution,
        t_to_infectious_dist::ContinuousUnivariateDistribution,
        t_to_pos_dist::ContinuousUnivariateDistribution,
        t_to_neg_dist::ContinuousUnivariateDistribution,
        t_sympt_start_dist::ContinuousUnivariateDistribution,
        t_sympt_end_dist::ContinuousUnivariateDistribution,
        prop_asympt_dist::ContinuousUnivariateDistribution,
        t_quarantine_start_dict::Dict)

A struct of model parameters for a contact quarantine simulation. For defaults see [`defaultContactPars`](@ref). Continuous distributions that are bounded on at least one side by a magnitude of ``\\infty`` should be truncated using [`truncated`](https://juliastats.org/Distributions.jl/stable/truncate/#Distributions.truncated) so that output metrics that are means are not accidentally skewed by rare large values.

# Arguments
- `IPD_pars::IPDPars`: Infectious period distribution, see [`IPDPars`](@ref).
- `test_sensitivity_dist::ContinuousUnivariateDistribution`: Distribution of a symptomatic individual's rapid antigen test (RAT) sensitivity - the probability that a RAT will return positive for a symptomatic quarantined contact while they're infectious (offset by `t_to_pos_dist` and `t_to_neg_dist`).
- `test_sensitivity_asympt_dist::ContinuousUnivariateDistribution`: Distribution of an asymptomatic individual's rapid antigen test (RAT) sensitivity - the probability that a RAT will return positive for an asymptomatic quarantine contact while they're infectious (offset by `t_to_pos_dist` and `t_to_neg_dist`).
- `t_iso_entry_dist::ContinuousUnivariateDistribution`: Distribution modelling an offset between the start of the isolation period (t=0) for the case that caused a quarantined contact and the start of that case's infectious period.
- `t_to_infectious_dist::ContinuousUnivariateDistribution`: Distribution modelling the serial interval - the time an infected contact would become infectious relative to the start of the index case's infectious period.
- `t_to_pos_dist::ContinuousUnivariateDistribution`: Distribution modelling the time it takes for an individual to be able to test positive on a RAT after the start of their infectious period.
- `t_to_neg_dist::ContinuousUnivariateDistribution`: Distribution modelling the time it takes for an individual to no longer test positive on a RAT after the end of their infectious period.
- `t_sympt_start_dist::ContinuousUnivariateDistribution`: Distribution modelling the time a quarantined contact's symptoms start relative to the start of their infectious period (if symptomatic).
- `t_sympt_end_dist::ContinuousUnivariateDistribution`: Distribution modelling the time a quarantined contact's symptoms end relative to the end of their infectious period (if symptomatic).
- `prop_asympt_dist::ContinuousUnivariateDistribution`: Distribution modelling the proportion of quarantined contacts that are asymptomatic per realisation.
- `t_quarantine_start_dict::Dict{String, Any}`: A dictionary which specifies the quarantine start method to be used in the simulation, as well as the additional required keys and values corresponding to each method. All keys must be of type `String`. Valid dictionary keys and types of the values are:
    - `"method" => m::Union{StartAtZero, StartFitData, StartProvideTestDelay}`: Quarantine start method to use. See [`StartAtZero`](@ref), [`StartFitData`](@ref) and [`StartProvideTestDelay`](@ref). __Required__.
    - `"quarantine_start_df" => df::DataFrame`: DataFrame containing an observed discrete distribution of the time a contact enters quarantine relative to ``t=0``, with columns, "entry\\_days" and "prob", where prob is the probability that the corresponding "entry\\_days" row entry is the time chosen for the contact to enter quarantine. Column "prob" will be normalised by [`MitigatingIsolationAndQuarantine.quarantineStartFit!`](@ref). __Required by [`StartFitData`](@ref)__. __Required by [`StartProvideTestDelay`](@ref) if `"show_fitted_distribution"` is `true`__.
    - `"case_test_delay_dist_mean" => num::Number`: The scale (expectation) of the exponential testing delay distribution to use. Must be greater than zero. __Required by [`StartProvideTestDelay`](@ref)__.
    - `"rounding_func" => func::Function`: A valid function that rounds a floating point number to an integer. `floor` or `round` are recommended. __Required by [`StartFitData`](@ref) and [`StartProvideTestDelay`](@ref)__.
    - `"show_fitted_distribution" => bool::Bool`: Whether to plot the fitted quarantine start distribution against the user provided empirical distribution for the [`StartFitData`](@ref) and [`StartProvideTestDelay`](@ref) methods. When "method" is [`StartFitData`](@ref), the user must set this to `false` to prevent this plot (not providing this key will still output the plot). In contrast, when "method" is [`StartProvideTestDelay`](@ref), the user must set this to `true` to output this plot (not providing this key will not output the plot).
"""
@option struct ContactDistributions

    IPD_pars::IPDPars
    test_sensitivity_dist::ContinuousUnivariateDistribution
    test_sensitivity_asympt_dist::ContinuousUnivariateDistribution
    t_iso_entry_dist::ContinuousUnivariateDistribution
    # serial interval
    t_to_infectious_dist::ContinuousUnivariateDistribution
    # time to testing positive after infectious
    t_to_pos_dist::ContinuousUnivariateDistribution
    # time to testing negative once no longer infectious
    t_to_neg_dist::ContinuousUnivariateDistribution
    # time symptoms start relative to infectious start
    t_sympt_start_dist::ContinuousUnivariateDistribution
    # time symptoms end relative to infectious end
    t_sympt_end_dist::ContinuousUnivariateDistribution
    # proportion of population asymptomatic
    prop_asympt_dist::ContinuousUnivariateDistribution
    # time contact enters quarantine relative to t=0
    t_quarantine_start_dict::Dict
end
