#=
A file containing functions for fitting a testing delay distribution to get
internal quarantine start times to fit empirical quarantine start times.

Author: Joel Trent
=#

"""
    plotXvY(x1::Vector, y1::Vector, x2::Vector, y2::Vector, label1::String,
        label2::String, mainTitle::String, title::String, xlab::String, ylab::String,
        outputFileName::String, save::Bool=false)

Plots a generic 2 way python plot of x values vs y values.

# Arguments
- `x1::Vector`: A 1D vector of x values for line 1.
- `y1::Vector`: A 1D vector of y values for line 1, (`length(x1)==length(y1)`).
- `x2::Vector`: A 1D vector of x values for line 2.
- `y2::Vector`: A 1D vector of y values for line 2, (`length(x2)==length(y2)`).
- `label1::String`: Legend label for line 1.
- `label2::String`: Legend label for line 2.
- `mainTitle::String`: Plot main title.
- `title::String`: Plot [sub] title. Placed underneath the mainTitle.
- `xlab::String`: Label for x axis.
- `ylab::String`: Label for y axis.
- `outputFileName::String`: The name and location to save the plot.
- `save::Bool=false`: Whether to save the plot.
"""
function plotXvY(x1::Vector, y1::Vector, x2::Vector, y2::Vector, label1::String,
    label2::String, mainTitle::String, title::String, xlab::String, ylab::String,
    outputFileName::String, save::Bool=false)
    # Seaborn.set_theme()
    set_style("ticks")
    Seaborn.set_color_codes("pastel")

    # Initialise plots - need figure size to make them square and nice
    fig = plt.figure(figsize=(6,5), dpi=300)

    Seaborn.plot(x1, y1, label=label1, lw = 2, marker=".")
    Seaborn.plot(x2, y2, label=label2, lw = 2, marker="x")

    plt.xlabel(xlab)
    plt.ylabel(ylab)

    plt.title(title)
    plt.suptitle(mainTitle)
    plt.legend(loc = "center right")
    plt.tight_layout()

    # required to display graph on plots.
    display(fig)

    if save
        # Save graph as pngW
        fig.savefig(outputFileName)
    end

    close()
end

"""
    plotXvLogY(x1::Vector, y1::Vector, x2::Vector, y2::Vector, label1::String,
        label2::String, mainTitle::String, title::String, xlab::String, ylab::String,
        outputFileName::String, save::Bool=false)

Plots a generic 2 way python plot of x values vs y values, with y axis on log scale.

# Arguments
- `x::Vector`: A 1D vector of x values.
- `y::Vector`: A 1D vector of y values (`length(x)==length(y)`).
- `x_best::Number`: A number representing the best value of `x` as given by `y`.
- `label1::String`: Legend label for the x-y line.
- `label2::String`: Legend label for the vertical line for `x_best`.
- `mainTitle::String`: Plot main title.
- `title::String`: Plot [sub] title. Placed underneath the mainTitle.
- `xlab::String`: Label for x axis.
- `ylab::String`: Label for y axis.
- `outputFileName::String`: The name and location to save the plot.
- `save::Bool=false`: Whether to save the plot.
"""
function plotXvLogY(x::Vector, y::Vector, x_best::Number, label1::String,
    label2::String, mainTitle::String, title::String, xlab::String, ylab::String,
    outputFileName::String, save::Bool=false)
    # Seaborn.set_theme()
    set_style("ticks")
    Seaborn.set_color_codes("pastel")

    # Initialise plots - need figure size to make them square and nice
    fig = plt.figure(figsize=(6,5), dpi=300)

    Seaborn.plot(x, y, label=label1, lw = 2)
    Seaborn.axvline(x_best, label=label2, linestyle="--")

    plt.yscale("log")

    plt.xlabel(xlab)
    plt.ylabel(ylab)

    plt.title(title)
    plt.suptitle(mainTitle)
    plt.legend(loc = "center right")
    plt.tight_layout()

    # required to display graph on plots.
    display(fig)

    if save
        # Save graph as pngW
        fig.savefig(outputFileName)
    end

    close()
end

"""
    countmapDF(array::Array)

A version of `StatsBase`'s `countmap` function that outputs a DataFrame instead of a dictionary, with columns for `days`, `count` of number of occurrences of each day and `proportion` based on the distribution of that `count`.

Used by [`MitigatingIsolationAndQuarantine.testingDelayDF`](@ref) to determine the distribution of quarantine start times produced using model distributions.

# Arguments
- `array::Array`: A 1D vector of discrete (integer) values.
"""
function countmapDF(array::Array)
    array_counts = countmap(array)
    array_keys = keys(array_counts)

    array_df = DataFrame("days" => zeros(length(array_keys)),
                            "count" => zeros(length(array_keys)),
                            "proportion" => zeros(length(array_keys)))

    row = 1
    for key in array_keys
        array_df[row, "days"] = Int64(key)
        array_df[row, "count"] = array_counts[key]
        row = row + 1
    end

    array_df[:, "proportion"] .= array_df[:, "count"] ./ sum(array_df[:, "count"])

    array_df = @chain array_df begin
        @orderby :days
    end

    return array_df
end

"""
    testingDelayDF(n::Int64, convolution_sorted::Array{Float64,1}, rounding_func::Function,
        θ::Number, index_of_smallest_neg::Int64)

Returns a DataFrame containing an approximation of the quarantine start time distribution that would be produced using the convolution of the model's isolation entry time relative to infectiousness distribution, time to be able to test positive on a RAT after becoming infectious distribution and an exponential testing delay distribution with mean `θ`.

# Arguments
- `n::Int64`: The number of distinct random draws to take from the testing delay distribution. Also the length of `convolution_sorted`.
- `convolution_sorted::Array{Float64,1}`: A sorted, smallest to largest, 1D vector containing the "convolution" of the model's isolation entry distribution (the time the first case detected becomes infectious relative to symptoms showing at ``t=0``, `model_pars["t_iso_entry_dist"]`) and time to positive distribution (time it takes for that case to be able to test positive on a RAT after the start of their infectious period, `model_pars["t_to_pos_dist"]`). Practically speaking this "convolution" is an array containing the element-wise addition of n independent random draws from each distribution.
- `rounding_func::Function`: A valid function that rounds a floating point number to an integer. `floor` or `round` are recommended.
- `θ::Number`: The scale (expectation) of the exponential testing delay distribution to use.
- `index_of_smallest_neg::Int64`: The index of the smallest magnitude negative value in `convolution_sorted`. I.e. `convolution_sorted[1:index_of_smallest_neg]` are all less than zero, and the remaining values are greater than zero.

# Details

We approximate the quarantine start time of households as the combination of, the time the first detected case becomes infectious relative to symptom onset (which is ``t=0``), plus the time it takes to be able to test positive on a RAT after becoming infectious, plus an exponentially distributed testing delay distribution modelling the additional time it takes for that case to test positive, causing quarantine to start for their contacts.

In New Zealand household data, the first detected case [almost] always is detected after or at the same time as symptom onset. Resultantly, we enforce that the quarantine start time for their household contacts must occur at or after ``t=0``. In our model we treat the time this first case is detected as the same as the quarantine start time of their household contacts. __This is not fully realistic__ as a case could test positive mid-way through the day while their household contacts are at work or in another location where they cannot immediately begin quarantine.

To enforce that the quarantine start time for all n elements of `convolution_sorted` is at least ``t=0``, we truncate the left side of the testing delay distribution for any `convolution_sorted` values that are negative to the positive magnitude of that negative value. Because the testing delay distribution we've chosen is exponential, which has special pdf properties, this truncation means we can just use the original testing delay distribution to draw quarantine start times directly for `convolution_sorted` elements that are negative. For elements that are not negative, the quarantine start time is the addition of those elements of `convolution_sorted` and random draws from the testing delay distribution.

To obtain discrete quarantine start times which we can use in our model and compare to New Zealand home quarantine data we apply a rounding function, `rounding_func`, to the quarantine start times produced previously.

The use of a rounding function to discretise these quarantine start times to integer values does introduce additional error in the exact quarantine start times the model uses relative to reality. For example, if `rounding_func` is `floor`, the model uses daily testing and our case develops symptoms at 6am and tests positive at 9pm (``t=\\frac{21-6}{24}=0.625``), the floating point quarantine start time of ``0.625`` will be rounded down to ``t=0``. In the model this case's contacts would be required to start quarantine and daily testing from ``t=0``, while in reality they may only begin quarantine or daily testing from ``t=0.625``. This is a design feature rather than a bug; this error will never exceed 1 day and will on average be at most 0.5 days in the model. This is largely for simplicity; contacts in the model can only start or end quarantine, be tested or be released for a day on integer days. This error may have a smaller impact if the `rounding_func` is `round`. This means that exact model outputs need to be __interpreted with caution__. The New Zealand data of binned quarantine start days that we have used to fit quarantine start times to has a similar rounding error (it ignores the time of the day, only comparing the number of calendar days).

Used by [`MitigatingIsolationAndQuarantine.testingDelayObj`](@ref) when `model_pars["t_quarantine_start_dict"]["method"] isa` [`StartFitData`](@ref) and used directly by [`MitigatingIsolationAndQuarantine.quarantineStartFit!`](@ref) when  `model_pars["t_quarantine_start_dict"]["method"] isa` [`StartProvideTestDelay`](@ref) and `model_pars["t_quarantine_start_dict"]["show_fitted_distribution"] == true`.

Uses [`MitigatingIsolationAndQuarantine.countmapDF`](@ref) to determine the distribution of quarantine start times produced using model distributions. The DataFrame returned by the call to [`MitigatingIsolationAndQuarantine.countmapDF`](@ref) is returned by this function.
"""
function testingDelayDF(n::Int64, convolution_sorted::Array{Float64,1}, rounding_func::Function,
    θ::Number, index_of_smallest_neg::Int64)

    test_delay_dist = Exponential(θ)

    approx_quarantine_start = zeros(n)
    approx_quarantine_start[1:index_of_smallest_neg] .= rand(test_delay_dist, index_of_smallest_neg)

    approx_quarantine_start[(index_of_smallest_neg+1):end] .=
        convolution_sorted[(index_of_smallest_neg+1):end] .+ rand(test_delay_dist, n-index_of_smallest_neg)

    discrete_quarantine_start = rounding_func.(approx_quarantine_start)
    discrete_quarantine_start_df = countmapDF(discrete_quarantine_start)

    return discrete_quarantine_start_df
end

"""
    testingDelayObj(n::Int64, convolution_sorted::Array{Float64,1}, index_of_smallest_neg::Int64,
        quarantine_start_df::DataFrame, rounding_func::Function, θ_vals::Array)

Given an array of `θ_vals` to use for the mean of the exponential testing delay distribution, this function returns the value of θ, `θ_best`, that minimises the objective: the sum of least squares differences between the approximate quarantine start distribution returned by [`MitigatingIsolationAndQuarantine.testingDelayDF`](@ref) and the empirical distribution defined in `quarantine_start_df`. Also returns the DataFrame of the approximate quarantine start distribution corresponding to `θ_best`, as well as an array of objectives that correspond to the objective for each element of `θ_vals`.

# Arguments
- `n::Int64`: The number of distinct random draws to take from the testing delay distribution. Also the length of `convolution_sorted`.
- `convolution_sorted::Array{Float64,1}`: A sorted, smallest to largest, 1D vector containing the "convolution" of the model's isolation entry distribution (the time the first case detected becomes infectious relative to symptoms showing at ``t=0``, `model_pars["t_iso_entry_dist"]`) and time to positive distribution (time it takes for that case to be able to test positive on a RAT after the start of their infectious period, `model_pars["t_to_pos_dist"]`). Practically speaking this "convolution" is an array containing the element-wise addition of n independent random draws from each distribution.
- `index_of_smallest_neg::Int64`: The index of the smallest magnitude negative value in `convolution_sorted`. I.e. `convolution_sorted[1:index_of_smallest_neg]` are all less than zero, and the remaining values are greater than zero.
- `quarantine_start_df::DataFrame`: DataFrame containing an observed discrete distribution of the time a contact enters quarantine relative to ``t=0``, with columns, "entry\\_days" and "prob", where prob is the probability that the corresponding "entry\\_days" row entry is the time chosen for the contact to enter quarantine.
- `rounding_func::Function`: A valid function that rounds a floating point number to an integer. `floor` or `round` are recommended.
- `θ_vals::Array`: A 1D array containing distinct scales (expectations) of the exponential testing delay distribution, to calculate an objective at.
"""
function testingDelayObj(n::Int64, convolution_sorted::Array{Float64,1}, index_of_smallest_neg::Int64,
    quarantine_start_df::DataFrame, rounding_func::Function, θ_vals::Array)

    smallest_obj = Inf64
    θ_best = θ_vals[1]
    best_quar_start_df = DataFrame()
    objectives = zeros(length(θ_vals))

    i = 1
    for θ in θ_vals

        discrete_quarantine_start_df = testingDelayDF(n, convolution_sorted, rounding_func, θ, index_of_smallest_neg)

        # least squares objective
        obj = 0.0
        for i in 1:nrow(quarantine_start_df)
            if quarantine_start_df[i, "entry_days"] in discrete_quarantine_start_df.days

                index = findfirst(quarantine_start_df[i, "entry_days"] .== discrete_quarantine_start_df.days)
                obj = obj + (quarantine_start_df[i,"prob"] - discrete_quarantine_start_df[index, "proportion"])^2

            else
                obj = obj + (quarantine_start_df[i,"prob"])^2
            end
        end

        objectives[i] = obj * 1.0

        if obj < smallest_obj
            smallest_obj = obj * 1.0
            θ_best = θ * 1.0
            best_quar_start_df = copy(discrete_quarantine_start_df)
        end

        i = i+1
    end

    return best_quar_start_df, θ_best, objectives
end

"""
    sortedConvolution(model_pars::Dict{String, Any})

Returns the sorted, from smallest to largest, "convolution" of the model's isolation entry distribution (the time the first case detected becomes infectious relative to symptoms showing at ``t=0``, `model_pars["t_iso_entry_dist"]`) and time to positive distribution (time it takes for that case to be able to test positive on a RAT after the start of their infectious period, `model_pars["t_to_pos_dist"]`). Practically speaking this "convolution" is a 1D array containing the element-wise addition of n independent random draws from each distribution. Also returns the length of the array, `n`, and the negative value in the sorted array with the largest index.

# Arguments
- `model_pars::Dict{String, Any}`: A dictionary of model parameter distributions to be used in the simulation in place of the default parameters in [`defaultContactPars`].
"""
function sortedConvolution(model_pars::Dict{String, Any})
    n=1000000
    convolution = rand(model_pars["t_iso_entry_dist"], n) .+ rand(model_pars["t_to_pos_dist"], n)
    convolution_sorted = sort(convolution)

    smallest_allowed_t = 0.0
    index_of_smallest_neg = sum(convolution_sorted .< smallest_allowed_t)

    return n, convolution_sorted, index_of_smallest_neg
end

"""
    fitTestingDelayDist!(model_pars::Dict{String, Any})

Determines the approximately best mean value, `θ_best`, for the exponential testing delay distribution, where best is defined as the value that minimises the sum of least squares difference between the approximate quarantine start distribution created using model distributions and the empirical distribution defined in `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]`, which we set to be the objective.

# Arguments
- `model_pars::Dict{String, Any}`: A dictionary of model parameter distributions to be used in the simulation in place of the default parameters in [`defaultContactPars`](@ref).

# Details
Here we are effectively determining the distribution, `case_test_delay_dist` that makes the convolution of the pdfs of `model_pars["t_iso_entry_dist"]`, `model_pars["t_to_pos_dist"]` and `case_test_delay_dist` match the provided discrete pdf of `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]`, with the values drawn from `case_test_delay_dist` also required to make this convolution only exist for non-negative values. This is done by drawing a large number of independent random values from each of these distributions ([`MitigatingIsolationAndQuarantine.sortedConvolution`](@ref) is used for the first two distributions), adding them together and comparing a discretised version of their distribution to `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]` (see [`MitigatingIsolationAndQuarantine.testingDelayObj`](@ref)).

The objective function used is assumed to be relatively convex such that a double grid search across parameter space can be performed to find an approximately best value. The approach we detail is not guaranteed to find a global optimum, but we are reasonably confident of a good approximation of a local optimum. The distribution which we are parameterising over is the case testing delay distribution which is exponential with parameter θ. Random draws from this distribution have units of days.

## Grid search 1
As θ is also the mean of the distribution, we can set some simple bounds on our first grid search that prevent θ from being non-sensical. Namely, the distribution we are fitting to is a quarantine start distribution with specified days. Due to how we've defined the quarantine start distribution in [`MitigatingIsolationAndQuarantine.testingDelayDF`](@ref), we know that the mean test delay in days should be between 0 and the maximum number of days in the quarantine start distribution. We then calculate the objectives across this range using a coarsely defined grid with step size of 0.2 days, which is passed to [`MitigatingIsolationAndQuarantine.testingDelayObj`](@ref).

## Grid search 2
Once we've obtained the objectives from our first gridsearch we find the θ that minimises these objectives, `θ_best_1` and try to use that as the midpoint for our second gridsearch which will be performed on a finer grid with step size of 0.002 days and have 1/4 of the range. The bounds are set so that the new range considered has 1/4 of the original range, is contained within the original range, contains `θ_best_1`, and `θ_best_1` is as close to the midpoint of the new range as possible. We then calculate the objectives across this new range using [`MitigatingIsolationAndQuarantine.testingDelayObj`](@ref) again and find the θ that minimises, `θ_best_2`. This approach assumes that there will be other good solutions in the range around our first best solution. A finer grid is not used because it would imply a greater level of accuracy than can be reasonably supported through this stochastic approach and it would require a higher cpu cost.

Modifies `model_pars` in place:
- The quarantine start discrete probability distribution `model_pars["t_quarantine_start_dict"]["quarantine_start_df"][:, "prob"]`, is normalised.
- The case testing delay distribution is set to be exponential with the best mean value obtained, `model_pars["t_quarantine_start_dict"]["case_test_delay_dist"] = Exponential(θ_best_2)`.

It returns the DataFrame of the approximate quarantine start distribution corresponding to `θ_best_2`, as well as `θ_best_2`.
"""
function fitTestingDelayDist!(model_pars::Dict{String, Any})

    println("Fitting case testing delay distribution to quarantine start times")

    # normalise quarantine start discrete distribution
    model_pars["t_quarantine_start_dict"]["quarantine_start_df"][:, "prob"] .=
            model_pars["t_quarantine_start_dict"]["quarantine_start_df"].prob ./
                sum(model_pars["t_quarantine_start_dict"]["quarantine_start_df"].prob)

    n, convolution_sorted, index_of_smallest_neg = sortedConvolution(model_pars)

    θ_min_1 = 0.0
    θ_max_1 = maximum(model_pars["t_quarantine_start_dict"]["quarantine_start_df"].entry_days)
    θ_range_1 = θ_min_1:0.2:θ_max_1
    θ_range_1 = θ_range_1[2:end]

    best_quar_start_df, θ_best, objectives_1 = testingDelayObj(n, convolution_sorted, index_of_smallest_neg,
                            model_pars["t_quarantine_start_dict"]["quarantine_start_df"],
                            model_pars["t_quarantine_start_dict"]["rounding_func"], collect(θ_range_1))

    if model_pars["t_quarantine_start_dict"]["show_objectives"]

        if haskey(model_pars["t_quarantine_start_dict"], "save_plots") &&
            model_pars["t_quarantine_start_dict"]["save_plots"] === true

            outputFileName = joinpath(model_pars["t_quarantine_start_dict"]["output_dir"],
                                        "testing_delay_grid1_"*model_pars["t_quarantine_start_dict"]["simulation_name"])

            plotXvLogY(collect(θ_range_1), objectives_1, θ_best,
                "Objectives", "Best θ = "*string(θ_best), "",
                "Log objective across θ values for grid search #1",
                "θ", "Log(objective)", outputFileName, true)
        else
            plotXvLogY(collect(θ_range_1), objectives_1, θ_best,
                "Objectives", "Best θ = "*string(θ_best), "",
                "Log objective across θ values for grid search #1",
                "θ", "Log(objective)", "", false)
        end
    end

    # Insert way of extracting good outer bounds from the above calculated objectives here

    # θ_1_df = DataFrame("θ"=collect(θ_range_1), "obj"=objectives)
    #
    # θ_1_df_sorted =  @chain θ_1_df begin
    #     @orderby :obj
    # end

    θ_midpoint_2 = θ_best
    interval_length = 0.25 * θ_max_1 # - θ_min_1

    # midpoint is too close to θ_min_1, so left boundary is θ_min_1
    if (θ_midpoint_2 - interval_length*0.5) < θ_min_1
        θ_min_2 = θ_min_1
        θ_max_2 = θ_min_1 + interval_length

    # midpoint is too close to θ_max_1, so right boundary is θ_max_1
    elseif (θ_midpoint_2 + interval_length*0.5) > θ_max_1
        θ_min_2 = θ_max_1 - interval_length
        θ_max_2 = θ_max_1

    else
        θ_min_2 = θ_midpoint_2 - interval_length*0.5
        θ_max_2 = θ_midpoint_2 + interval_length*0.5
    end

    θ_range_2 = θ_min_2:0.002:θ_max_2
    θ_range_2 = θ_range_2[2:end]

    best_quar_start_df, θ_best, objectives_2 = testingDelayObj(n, convolution_sorted, index_of_smallest_neg,
                            model_pars["t_quarantine_start_dict"]["quarantine_start_df"],
                            model_pars["t_quarantine_start_dict"]["rounding_func"], collect(θ_range_2))

    if model_pars["t_quarantine_start_dict"]["show_objectives"]

        if haskey(model_pars["t_quarantine_start_dict"], "save_plots") &&
            model_pars["t_quarantine_start_dict"]["save_plots"] === true

            outputFileName = joinpath(model_pars["t_quarantine_start_dict"]["output_dir"],
                                        "testing_delay_grid2_"*model_pars["t_quarantine_start_dict"]["simulation_name"])

            plotXvLogY(collect(θ_range_2), objectives_2, θ_best,
                "Objectives", "Best θ = "*string(θ_best), "",
                "Log objective across θ values for grid search #2",
                "θ", "Log(objective)", outputFileName, true)
        else
            plotXvLogY(collect(θ_range_2), objectives_2, θ_best,
                "Objectives", "Best θ = "*string(θ_best), "",
                "Log objective across θ values for grid search #2",
                "θ", "Log(objective)", "", false)
        end
    end

    # modify model_pars to now contain the specified test delay distribution
    model_pars["t_quarantine_start_dict"]["case_test_delay_dist"] = Exponential(θ_best)

    return best_quar_start_df, θ_best
end

"""
    quarantineStartFit!(model_pars::Dict{String, Any})

Called by [`masterContactSimulation`](@ref) to check if the quarantine start time method is different than the default [`StartAtZero`](@ref), and performs any necessary actions required by the method in question (see Details).

# Arguments
- `model_pars::Dict{String, Any}`: A dictionary of model parameter distributions to be used in the simulation in place of the default parameters in [`defaultContactPars`](@ref).

# Details
If `model_pars["t_quarantine_start_dict"]` has no key, "method", then an argument error will be thrown. If the value of `model_pars["t_quarantine_start_dict"]["method"]` is not of type [`StartAtZero`](@ref), [`StartFitData`](@ref) or [`StartProvideTestDelay`](@ref) then an argument error will be thrown as well.

## Method has type StartAtZero
`model_pars` will not be modified and the function will return `true` (the simulation can be continued). For method meaning see [`StartAtZero`](@ref).

## Method has type StartFitData
If `model_pars["t_quarantine_start_dict"]["method"] isa` [`StartFitData`](@ref), it will first check if keys "quarantine\\_start\\_df" and "rounding\\_func" are provided in `model_pars["t_quarantine_start_dict"]` and have the values specified in [`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref). If not, an assertion error will be thrown.

Otherwise, [`MitigatingIsolationAndQuarantine.fitTestingDelayDist!`](@ref) will be called to determine the an approximate best value of θ, the mean of a exponential distribution modelling the testing delay of the first detected case in a household, that makes an approximate quarantine start time distribution match the distribution given in `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]`. [`MitigatingIsolationAndQuarantine.fitTestingDelayDist!`](@ref) will also normalise `model_pars["t_quarantine_start_dict"]["quarantine_start_df"][:, "prob"]`. The methodology used for the approximate distribution is covered in [`MitigatingIsolationAndQuarantine.testingDelayDF`] and is carried over into the actual simulation within [`MitigatingIsolationAndQuarantine.initPopDataframeContact`]. `model_pars` is modified to now include: `model_pars["t_quarantine_start_dict"]["case_case_test_delay_dist"] = Exponential(θ)`.

Once the case testing delay distribution is fitted, unless `model_pars["t_quarantine_start_dict"]["show_fitted_distribution"] == false`, the approximate quarantine start distribution will be output to the user against the distribution given in `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]` using [`MitigatingIsolationAndQuarantine.plotXvY`](@ref), to check whether they are happy with it. If the key, "show\\_fitted\\_distribution", is not provided then the fitted distribution will still be output. If the user is happy with the fit `true` will be returned and the simulation will continue. Else `false` is returned and the simulation will stop.

## Method has type StartProvideTestDelay
If `model_pars["t_quarantine_start_dict"]["method"] isa` [`StartProvideTestDelay`](@ref), it will first check if keys "case\\_test\\_delay\\_dist\\_mean" and "rounding\\_func" are provided in `model_pars["t_quarantine_start_dict"]` and have the values specified in [`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref). If not, an assertion error will be thrown. The value for key "case\\_test\\_delay\\_dist\\_mean" must be greater than zero.

Otherwise, `model_pars` is modified to now include: `model_pars["t_quarantine_start_dict"]["case_case_test_delay_dist"] = Exponential(θ)`, where `θ = model_pars["t_quarantine_start_dict"]["case_test_delay_dist_mean"]`.

If `model_pars["t_quarantine_start_dict"]["show_fitted_distribution"] == true`, it will first check if key, "quarantine\\_start\\_df", is provided with the correct type in `model_pars["t_quarantine_start_dict"]`. If so, it will normalise `model_pars["t_quarantine_start_dict"]["quarantine_start_df"][:, "prob"]`. Then the approximate quarantine start distribution will be calculated using [`MitigatingIsolationAndQuarantine.sortedConvolution`](@ref) and [`MitigatingIsolationAndQuarantine.testingDelayDF`](@ref), and will be output to the user against the distribution given in `model_pars["t_quarantine_start_dict"]["quarantine_start_df"]` using [`MitigatingIsolationAndQuarantine.plotXvY`](@ref), to check whether they are happy with it. If the key, "show\\_fitted\\_distribution", is not provided then the fitted distribution will not be output. If the user is happy with the fit `true` will be returned and the simulation will continue. Else `false` is returned and the simulation will stop.
"""
function quarantineStartFit!(model_pars::Dict{String, Any})

    if haskey(model_pars["t_quarantine_start_dict"], "method")

        if model_pars["t_quarantine_start_dict"]["method"] isa StartAtZero
            # do nothing
            return true
        elseif model_pars["t_quarantine_start_dict"]["method"] isa StartProvideTestDelay

            testing_delay_tests = haskey(model_pars["t_quarantine_start_dict"], "case_test_delay_dist_mean") &&
                                    model_pars["t_quarantine_start_dict"]["case_test_delay_dist_mean"] isa Number &&
                                    model_pars["t_quarantine_start_dict"]["case_test_delay_dist_mean"] > 0.0

            @assert testing_delay_tests "A number greater than zero for the mean of an exponential distribution must be provided for this \"method\" in model_pars[\"t_quarantine_start_dict\"], with key \"case_test_delay_dist_mean\"."

            @assert haskey(model_pars["t_quarantine_start_dict"], "rounding_func") "A rounding function to use for discretising quarantine start times must be provided for this \"method\" in model_pars[\"t_quarantine_start_dict\"], with key \"rounding_func\". Either 'floor' or 'round' is recommended."

            model_pars["t_quarantine_start_dict"]["case_test_delay_dist"] = Exponential(
                model_pars["t_quarantine_start_dict"]["case_test_delay_dist_mean"]
                )

            if (haskey(model_pars["t_quarantine_start_dict"], "show_fitted_distribution")) && model_pars["t_quarantine_start_dict"]["show_fitted_distribution"] == true

                @assert haskey(model_pars["t_quarantine_start_dict"], "quarantine_start_df") "To visualise the fitted distribution against an empirical distribution a DataFrame must be provided for this \"method\" in model_pars[\"t_quarantine_start_dict\"], with key \"quarantine_start_df\" and columns \"entry_days\" and \"prob\". If visualisation is not desired set model_pars[\"t_quarantine_start_dict\"][\"show_fitted_distribution\"]==false."

                # make sure we have the required distributions to perform the fit.
                if !haskey(model_pars, "t_iso_entry_dist")
                    model_pars["t_iso_entry_dist"] = defaultContactPars()["t_iso_entry_dist"]
                end
                if !haskey(model_pars, "t_to_pos_dist")
                    model_pars["t_to_pos_dist"] = defaultContactPars()["t_to_pos_dist"]
                end
                n, convolution_sorted, index_of_smallest_neg = sortedConvolution(model_pars)

                quar_start_df = testingDelayDF(n, convolution_sorted,
                                    model_pars["t_quarantine_start_dict"]["rounding_func"],
                                    model_pars["t_quarantine_start_dict"]["case_test_delay_dist_mean"],
                                    index_of_smallest_neg)

                if haskey(model_pars["t_quarantine_start_dict"], "save_plots") &&
                    model_pars["t_quarantine_start_dict"]["save_plots"] === true

                    outputFileName = joinpath(model_pars["t_quarantine_start_dict"]["output_dir"],
                                                "quarantine_start_fit_"*model_pars["t_quarantine_start_dict"]["simulation_name"])

                    plotXvY(model_pars["t_quarantine_start_dict"]["quarantine_start_df"].entry_days,
                        model_pars["t_quarantine_start_dict"]["quarantine_start_df"].prob ./ sum(model_pars["t_quarantine_start_dict"]["quarantine_start_df"].prob),
                        quar_start_df.days, quar_start_df.proportion,
                        "Data", "Fit", "",
                        "Testing delay is exponential with θ = "*string(model_pars["t_quarantine_start_dict"]["case_test_delay_dist_mean"]),
                        "Days", "Proportion starting quarantine", outputFileName, true)
                else
                    plotXvY(model_pars["t_quarantine_start_dict"]["quarantine_start_df"].entry_days,
                        model_pars["t_quarantine_start_dict"]["quarantine_start_df"].prob ./ sum(model_pars["t_quarantine_start_dict"]["quarantine_start_df"].prob),
                        quar_start_df.days, quar_start_df.proportion,
                        "Data", "Fit", "",
                        "Testing delay is exponential with θ = "*string(θ_best),
                        "Days", "Proportion starting quarantine", "", false)
                end

                println("The output plot shows the distribution of quarantine start times in the model with the provided testing delay distribution vs the empirical distribution.")
                println("Are you happy with this fit; do you wish to continue? (Y to continue)")
                input_val = readline()
                println()

                if lowercase(input_val) == "y"
                    return true
                else
                    return false
                end
            end

            # nothing else is required
            return true

        elseif model_pars["t_quarantine_start_dict"]["method"] isa StartFitData
            @assert haskey(model_pars["t_quarantine_start_dict"], "quarantine_start_df") "A DataFrame must be provided for this \"method\" in model_pars[\"t_quarantine_start_dict\"], with key \"quarantine_start_df\" and columns \"entry_days\" and \"prob\"."

            dataframe_tests = model_pars["t_quarantine_start_dict"]["quarantine_start_df"] isa DataFrame &&
                                "entry_days" in names(model_pars["t_quarantine_start_dict"]["quarantine_start_df"]) &&
                                "prob" in names(model_pars["t_quarantine_start_dict"]["quarantine_start_df"])

            @assert dataframe_tests "A DataFrame must be provided for this \"method\" in model_pars[\"t_quarantine_start_dict\"], with key \"quarantine_start_df\" and columns \"entry_days\" and \"prob\"."

            @assert haskey(model_pars["t_quarantine_start_dict"], "rounding_func") "A rounding function to use for discretising quarantine start times must be provided for this \"method\" in model_pars[\"t_quarantine_start_dict\"], with key \"rounding_func\". Either 'floor' or 'round' is recommended."

            # fit a testing delay distribution to empirical data. Ask user if they are happy with the fit.
            # make sure we have the required distributions to perform the fit.
            if !haskey(model_pars, "t_iso_entry_dist")
                model_pars["t_iso_entry_dist"] = defaultContactPars()["t_iso_entry_dist"]
            end
            if !haskey(model_pars, "t_to_pos_dist")
                model_pars["t_to_pos_dist"] = defaultContactPars()["t_to_pos_dist"]
            end

            if !haskey(model_pars["t_quarantine_start_dict"], "show_objectives")
                model_pars["t_quarantine_start_dict"]["show_objectives"] = false
            else
                @assert model_pars["t_quarantine_start_dict"]["show_objectives"] isa Bool "Key \"show_objectives\" for this \"method\" in model_pars[\"t_quarantine_start_dict\"] must be a Boolean value: either false or true."
            end

            best_quar_start_df, θ_best = fitTestingDelayDist!(model_pars)

            if !(haskey(model_pars["t_quarantine_start_dict"], "show_fitted_distribution")) || model_pars["t_quarantine_start_dict"]["show_fitted_distribution"] == true


                if haskey(model_pars["t_quarantine_start_dict"], "save_plots") &&
                    model_pars["t_quarantine_start_dict"]["save_plots"] === true

                    outputFileName = joinpath(model_pars["t_quarantine_start_dict"]["output_dir"],
                                                "quarantine_start_fit_"*model_pars["t_quarantine_start_dict"]["simulation_name"])

                    plotXvY(model_pars["t_quarantine_start_dict"]["quarantine_start_df"].entry_days,
                        model_pars["t_quarantine_start_dict"]["quarantine_start_df"].prob,
                        best_quar_start_df.days, best_quar_start_df.proportion,
                        "Data", "Best Fit", "",
                        "Testing delay is exponential with θ = "*string(θ_best),
                        "Days", "Proportion starting quarantine", outputFileName, true)
                else
                    plotXvY(model_pars["t_quarantine_start_dict"]["quarantine_start_df"].entry_days,
                        model_pars["t_quarantine_start_dict"]["quarantine_start_df"].prob,
                        best_quar_start_df.days, best_quar_start_df.proportion,
                        "Data", "Best Fit", "",
                        "Testing delay is exponential with θ = "*string(θ_best),
                        "Days", "Proportion starting quarantine", "", false)
                end

                println("The output plot shows the distribution of quarantine start times in the model with the fitted testing delay distribution vs the empirical distribution.")
                println("Are you happy with this fit; do you wish to continue? (Y to continue)")
                input_val = readline()
                println()

                if lowercase(input_val) == "y"
                    return true
                else
                    return false
                end
            end

            return true
        else
            throw(ArgumentError("model_pars[\"t_quarantine_start_dict\"][\"method\"] must have type StartAtZero, StartFitData or StartProvideTestDelay"))
        end

    else
        throw(ArgumentError("model_pars[\"t_quarantine_start_dict\"] is required to have key \"method\" of type StartAtZero, StartFitData or StartProvideTestDelay. "))
    end

    return false
end
