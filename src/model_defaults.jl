#=
A file containing functions for constructing model parameters.

Author: Joel Trent
=#

"""
    defaultCasePars()::Dict{String,Any}

Returns a dictionary containing the default distribution values (parameters) for the case isolation simulation.

Distribution meanings are covered in [`CaseDistributions`](@ref).
"""
function defaultCasePars()::Dict{String,Any}
    default_pars =
        Dict(
                "IPD_pars" => IPDPars(2.0,0.1,2.1,0.1),
                "test_sensitivity_dist" => Uniform(0.7,0.8),
                "t_iso_entry_dist" => truncated(Normal(0.0, 0.3), -3.0, 3.0),
                "t_to_pos_dist"    => truncated(Weibull(2.0, 1.25), 0.0, 0.0), # truncation on [0, 4] recommended for this distribution if want non-zeros
                "t_to_neg_dist"    => truncated(Weibull(2.0, 1.8), 0.0, 0.0) # truncation on [0, 4] recommended for this distribution if want non-zeros
            )

    return default_pars
end

"""
    defaultContactPars()::Dict{String,Any}

Returns a dictionary containing the default distribution values (parameters) for the contact quarantine simulation.

Distribution meanings are covered in [`ContactDistributions`](@ref).
"""
function defaultContactPars()::Dict{String,Any}
    default_pars =
        Dict(
                "IPD_pars" => IPDPars(2.0,0.1,2.1,0.1),
                "test_sensitivity_dist" => Uniform(0.75,0.85),
                "test_sensitivity_asympt_dist" => Uniform(0.65,0.75),
                "t_to_infectious_dist" => truncated(Weibull(1.35, 3.5), 0.0, 14.0),
                "t_iso_entry_dist" => truncated(Normal(0.0, 0.0), -3.0, 3.0),
                "t_to_pos_dist"   => truncated(Weibull(2.0, 1.25), 0.0, 4.0),
                "t_to_neg_dist"   => truncated(Weibull(2.0, 1.8), 0.0, 4.0),
                "t_sympt_start_dist" => truncated(Normal(0, 1.5), -3, 2),
                "t_sympt_end_dist" => Normal(-1.0, 1.0),
                "prop_asympt_dist" => Uniform(0.35, 0.45),
                "t_quarantine_start_dict" => Dict("method" => StartAtZero(), # can be StartAtZero(), StartFitData or StartProvideTestDelay
                                                    "quarantine_start_df" => nothing,
                                                    "case_test_delay_dist_mean" => 1e-10,
                                                    "rounding_func" => nothing, # recommend floor or round
                                                    "show_fitted_distribution" => false
                                                    )
            )

    return default_pars
end

"""
    createModelStruct(sim_type::Union{CaseSimulation, ContactSimulation},
        model_pars::Dict{String, Any}=Dict{String,Any}()
        )::Union{CaseDistributions, ContactDistributions}

Constructs a struct of model parameters for a case isolation ([`MitigatingIsolationAndQuarantine.CaseDistributions`](@ref)) or contact quarantine ([`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref)) simulation.

This is done using provided parameter values in `model_pars` in conjunction with model defaults if a parameter was not specified in `model_pars`. Model defaults are defined in [`defaultCasePars`](@ref) for case isolation and [`defaultContactPars`](@ref) for contact quarantine. Continuous distributions that are bounded on at least one side by a magnitude of ``\\infty`` should be truncated using [`truncated`](https://juliastats.org/Distributions.jl/stable/truncate/#Distributions.truncated) so that output metrics that are means are not accidentally skewed by rare large values.

# Arguments
- `sim_type::Union{CaseSimulation, ContactSimulation}`: A `struct` for whether the simulation is for case isolation [`MitigatingIsolationAndQuarantine.CaseSimulation`](@ref) or contact quarantine [`MitigatingIsolationAndQuarantine.ContactSimulation`](@ref).
- `model_pars::Dict{String, Any}=Dict{String,Any}()`: A dictionary containing user defined parameter values/distributions for given simulation (default is an empty dictionary). All `model_pars` keys should be lowercase and identical to those specified in [`defaultCasePars`](@ref) or [`defaultContactPars`](@ref) (which ever one is relevant). All values (apart from for key "IPD_pars" which must be of type [`IPDPars`](@ref)) should have type `ContinuousUnivariateDistribution`.

# Example Use

```@repl
julia> using MitigatingIsolationAndQuarantine: createModelStruct, CaseSimulation, IPDPars
julia> using Distributions

julia> model_pars = Dict(
                        "IPD_pars" => IPDPars(2.0,0.1,2.1,0.1),
                        "test_sensitivity_dist" => Uniform(0.7,0.8)
                        )
Dict{String, Any} with 2 entries:
  "IPD_pars"              => IPDPars(2.0, 0.1, 2.1, 0.1)
  "test_sensitivity_dist" => Uniform{Float64}(a=0.7, b=0.8)

julia> model_struct = createModelStruct(CaseSimulation(), model_pars)
MitigatingIsolationAndQuarantine.CaseDistributions(
    IPDPars(2.0, 0.1, 2.1, 0.1),
    Uniform{Float64}(a=0.7, b=0.8),
    Truncated(Normal{Float64}(μ=0.0, σ=0.3); lower=-3.0, upper=3.0),
    Truncated(Weibull{Float64}(α=2.0, θ=1.25); lower=0.0, upper=0.0),
    Truncated(Weibull{Float64}(α=2.0, θ=1.8); lower=0.0, upper=0.0)
    )
```
"""
function createModelStruct(sim_type::Union{CaseSimulation, ContactSimulation},
    model_pars::Dict{String, Any}=Dict{String,Any}()
    )::Union{CaseDistributions, ContactDistributions}

    if sim_type isa CaseSimulation
        model_pars_default = defaultCasePars()
    else
        model_pars_default = defaultContactPars()
    end

    combined_model_pars = Dict{String,Any}()

    # if no parameters specified, build struct from defaults
    if isempty(model_pars)
        combined_model_pars = model_pars_default

    # combine dictionaries - if a parameter is not in the supplied model_pars, use
    # the default parameter for the struct
    else
        for key in keys(model_pars_default)
            if haskey(model_pars, key)
                combined_model_pars[key] = model_pars[key]
            else
                combined_model_pars[key] = model_pars_default[key]
            end
        end
    end

    if sim_type isa CaseSimulation
        model_pars_struct = from_dict(CaseDistributions, combined_model_pars)
    else
        model_pars_struct = from_dict(ContactDistributions, combined_model_pars)
    end

    return model_pars_struct
end
