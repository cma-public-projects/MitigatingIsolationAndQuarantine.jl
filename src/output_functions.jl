#=
A file containing functions for dealing with outputs and writing of metrics
from the isolation simulations.

Author: Joel Trent
=#

"""
    printTimeElapsed(time_elapsed::Float64)

Prints time elapsed as a nicely formatted period.

# Arguments
-`time_elapsed::Float64`: The time taken to execute a simulation run in seconds (can have decimal places).
"""
function printTimeElapsed(time_elapsed::Float64)
    println("Total time taken of: ", Dates.canonicalize(Dates.Second(convert(Int64, round(time_elapsed)))),".")
    return nothing
end

"""
    extractPropDailyMetrics(prop_daily_release::Array{Float64,2},
        max_iso_end_day::Int64,
        quant_l::Float64, quant_u::Float64)::DataFrame

Computes the mean and 95% quantile intervals (`quant_l` to `quant_u`) for the proportion of individuals released on each day. Returns these values in a DataFrame.

# Arguments
- `prop_daily_release::Array{Float64,2}`: An 2D array containing distinct simulation runs in columns and rows for each day (1 to `t_iso_end` for a given simulation run). See [`casePolicySimulation`](@ref) and [`contactPolicySimulation`](@ref).
- `max_iso_end_day::Int64`: The maximum `t_iso_end` day defined by any policy ([`TestPolicy`](@ref) or [`TestPolicyContact`](@ref)) in a given simulation.
- `quant_l::Float64`: The lower quantile statistic to compute.
- `quant_u::Float64`: The upper quantile statistic to compute.

# Details
For case isolation simulations this function is called multiple times per policy and for contact quarantine simulations it is called once per policy. Because policies can have different values of `t_iso_end` we compute outputs based on the maximum `t_iso_end` of any policy, to make the computation and storage of global simulation metrics simpler.
"""
function extractPropDailyMetrics(prop_daily_release::Array{Float64,2}, max_iso_end_day::Int64,
    quant_l::Float64, quant_u::Float64)::DataFrame
    #=
    Computes the mean and 95% confidence intervals for the proportion of individuals
    released on each day.
    =#

    prop_daily_mean = zeros(max_iso_end_day)
    prop_daily_l = zeros(max_iso_end_day)
    prop_daily_u = zeros(max_iso_end_day)

    prop_daily_mean[1:size(prop_daily_release,1)] = mean(prop_daily_release, dims=2)

    for day in 1:size(prop_daily_release,1)
        prop_daily_l[day] = quantile(prop_daily_release[day, :], quant_l)
        prop_daily_u[day] = quantile(prop_daily_release[day, :], quant_u)
    end

    return DataFrame(prop_daily_mean=prop_daily_mean, prop_daily_l=prop_daily_l,
                    prop_daily_u=prop_daily_u)
end

"""
    computeGlobMetrics!(global_metric_df::DataFrame, prop_daily_release_df::DataFrame,
        metric_df::DataFrame,
        prop_daily_release::Array{Float64,2}, prop_daily_release_infectious::Array{Float64,2},
        prop_daily_release_not_infectious::Array{Float64,2},
        prop_daily_not_release_infectious::Array{Float64,2},
        prop_daily_not_release_not_infectious::Array{Float64,2},
        index::Int64, sim_type::CaseSimulation)

For each policy defined in a case isolation simulation this function computes the mean and 95% quantiles for all statistics of interest. It operates in place on `global_metric_df` and `prop_daily_release_df`.

# Arguments
- `global_metric_df::DataFrame`: DataFrame which will contain the mean and 95% quantiles for statistics of interest from each policy run of a case isolation simulation. Entries are modified by this function.
- `prop_daily_release_df::DataFrame`: DataFrame which will contain the mean and 95% quantiles for statistics of interest related to the proportion of cases infectious and not infectious, and released and not released, on each day of isolation from each policy run of a case isolation simulation. Entries are modified by this function.
- `metric_df::DataFrame`: DataFrame containing point statistics from all iterations (in rows) for all statistics of interest (in columns) of a single policy run of a case isolation simulation.
- `prop_daily_release::Array{Float64,2}`: An 2D array of the proportion of all individuals released from isolation daily, containing distinct simulation runs in columns and rows for each day (1 to `t_iso_end` for a given `policy`).
- `prop_daily_release_infectious::Array{Float64,2}`: An 2D array of the proportion of all individuals released from isolation on a given day that were infectious on that day, containing distinct simulation runs in columns and rows for each day (1 to `t_iso_end` for a given `policy`).
- `prop_daily_release_not_infectious::Array{Float64,2}`: An 2D array of the proportion of all individuals released from isolation on a given day that were not infectious on that day, containing distinct simulation runs in columns and rows for each day (1 to `t_iso_end` for a given `policy`).
- `prop_daily_not_release_infectious::Array{Float64,2}`: An 2D array of the proportion of all individuals not yet released from isolation on a given day that were infectious on that day, containing distinct simulation runs in columns and rows for each day (1 to `t_iso_end` for a given `policy`).
- `prop_daily_not_release_not_infectious::Array{Float64,2}`: An 2D array of the proportion of all individuals not yet released from isolation on a given day that were not infectious on that day, containing distinct simulation runs in columns and rows for each day (1 to `t_iso_end` for a given `policy`).
- `index::Int64`: The policy number the simulation is up to, corresponding to the equivalent row index in `global_metric_df` and used in conjunction with `max_iso_end_day` (computed within the function) to determine the relevant row indexes of `prop_daily_release_df`.
- `sim_type::CaseSimulation`: A `struct` specifying the simulation is for case isolation. Unused by the function other than to ensure that Julia's multiple dispatch calls this function rather the one for the contact quarantine simulation.

# Details
As mentioned in [`extractPropDailyMetrics`](@ref) policies can have different values of `t_iso_end` so we compute outputs for `prop_daily_release_df` based on the maximum `t_iso_end` of any policy, to make the computation and storage of global simulation metrics simpler.

!!! tip "Proportion released arrays"
    For a given day the sum of `prop_released_infectious_mean` and `prop_release_not_infectious_mean`, across all previous days inclusive of the current day, plus the sum of `prop_not_release_infectious_mean` and `prop_not_release_not_infectious_mean` on the current day, is 1. If no cases have been released yet, then the sum of `prop_not_release_infectious_mean` and `prop_not_release_not_infectious_mean` on the current day is 1. The sum across all days of `prop_daily_release` is also 1; all cases are released by `t_iso_end` at a maximum.
"""
function computeGlobMetrics!(global_metric_df::DataFrame, prop_daily_release_df::DataFrame,
    metric_df::DataFrame,
    prop_daily_release::Array{Float64,2}, prop_daily_release_infectious::Array{Float64,2},
    prop_daily_release_not_infectious::Array{Float64,2},
    prop_daily_not_release_infectious::Array{Float64,2},
    prop_daily_not_release_not_infectious::Array{Float64,2},
    index::Int64, sim_type::CaseSimulation)

    quant_l = 0.025
    quant_u = 0.975

    # Global metrics ###########################################################
    # classifying columns
    # global_metric_df[index, :num_consec_tests] = numTestsToInt(policy.num_tests)
    # global_metric_df[index, :day_early_release] = policy.t_early_release * 1
    # global_metric_df[index, :day_iso_end] = policy.t_iso_end * 1

    # metric columns
    global_metric_df[index, :prop_released_infectious_mean] = mean(metric_df.prop_released_infectious)
    global_metric_df[index, :prop_released_infectious_l] = quantile(metric_df.prop_released_infectious, quant_l)
    global_metric_df[index, :prop_released_infectious_u] = quantile(metric_df.prop_released_infectious, quant_u)

    global_metric_df[index, :hours_infectious_after_release_mean] = mean(metric_df.t_infectious_after_release_mean)
    global_metric_df[index, :hours_infectious_after_release_l] = quantile(metric_df.t_infectious_after_release_mean, quant_l)
    global_metric_df[index, :hours_infectious_after_release_u] = quantile(metric_df.t_infectious_after_release_mean, quant_u)

    global_metric_df[index, :hours_infectious_after_release_population_mean] = mean(metric_df.t_infectious_after_release_population_mean)
    global_metric_df[index, :hours_infectious_after_release_population_l] = quantile(metric_df.t_infectious_after_release_population_mean, quant_l)
    global_metric_df[index, :hours_infectious_after_release_population_u] = quantile(metric_df.t_infectious_after_release_population_mean, quant_u)

    global_metric_df[index, :hours_excess_iso_per_person_mean] = mean(metric_df.t_excess_iso_per_person_mean)
    global_metric_df[index, :hours_excess_iso_per_person_l] = quantile(metric_df.t_excess_iso_per_person_mean, quant_l)
    global_metric_df[index, :hours_excess_iso_per_person_u] = quantile(metric_df.t_excess_iso_per_person_mean, quant_u)

    global_metric_df[index, :tests_used_per_case_mean] = mean(metric_df.tests_used_per_case_mean)
    global_metric_df[index, :tests_used_per_case_l] = quantile(metric_df.tests_used_per_case_mean, quant_l)
    global_metric_df[index, :tests_used_per_case_u] = quantile(metric_df.tests_used_per_case_mean, quant_u)

    global_metric_df[index, :days_in_isolation_mean] = mean(metric_df.t_in_isolation_mean)
    global_metric_df[index, :days_in_isolation_l] = quantile(metric_df.t_in_isolation_mean, quant_l)
    global_metric_df[index, :days_in_isolation_u] = quantile(metric_df.t_in_isolation_mean, quant_u)

    # prop daily ###############################################################
    max_iso_end_day = maximum(prop_daily_release_df.day_iso_end)

    end_index = max_iso_end_day * index
    start_index = end_index - max_iso_end_day + 1

    # total released daily
    prop_metrics = extractPropDailyMetrics(prop_daily_release, max_iso_end_day,
                                            quant_l, quant_u)

    # assign values
    prop_daily_release_df[start_index:end_index, :prop_released_mean] .= prop_metrics.prop_daily_mean
    prop_daily_release_df[start_index:end_index, :prop_released_l] .= prop_metrics.prop_daily_l
    prop_daily_release_df[start_index:end_index, :prop_released_u] .= prop_metrics.prop_daily_u

    # total released daily while infectious
    prop_metrics = extractPropDailyMetrics(prop_daily_release_infectious, max_iso_end_day,
                                            quant_l, quant_u)

    # assign values
    prop_daily_release_df[start_index:end_index, :prop_released_infectious_mean] .= prop_metrics.prop_daily_mean
    prop_daily_release_df[start_index:end_index, :prop_released_infectious_l] .= prop_metrics.prop_daily_l
    prop_daily_release_df[start_index:end_index, :prop_released_infectious_u] .= prop_metrics.prop_daily_u

    # total released daily while not infectious
    prop_metrics = extractPropDailyMetrics(prop_daily_release_not_infectious, max_iso_end_day,
                                            quant_l, quant_u)

    # assign values
    prop_daily_release_df[start_index:end_index, :prop_released_not_infectious_mean] .= prop_metrics.prop_daily_mean
    prop_daily_release_df[start_index:end_index, :prop_released_not_infectious_l] .= prop_metrics.prop_daily_l
    prop_daily_release_df[start_index:end_index, :prop_released_not_infectious_u] .= prop_metrics.prop_daily_u

    # total not released daily and not infectious
    prop_metrics = extractPropDailyMetrics(prop_daily_not_release_not_infectious, max_iso_end_day,
                                            quant_l, quant_u)

    # assign values
    prop_daily_release_df[start_index:end_index, :prop_not_released_not_infectious_mean] .= prop_metrics.prop_daily_mean
    prop_daily_release_df[start_index:end_index, :prop_not_released_not_infectious_l] .= prop_metrics.prop_daily_l
    prop_daily_release_df[start_index:end_index, :prop_not_released_not_infectious_u] .= prop_metrics.prop_daily_u

    # total not released daily and still infectious
    prop_metrics = extractPropDailyMetrics(prop_daily_not_release_infectious, max_iso_end_day,
                                            quant_l, quant_u)

    # assign values
    prop_daily_release_df[start_index:end_index, :prop_not_released_infectious_mean] .= prop_metrics.prop_daily_mean
    prop_daily_release_df[start_index:end_index, :prop_not_released_infectious_l] .= prop_metrics.prop_daily_l
    prop_daily_release_df[start_index:end_index, :prop_not_released_infectious_u] .= prop_metrics.prop_daily_u

    return nothing
end

"""
    computeGlobMetrics!(global_metric_df::DataFrame, prop_daily_detect_df::DataFrame,
        metric_df::DataFrame,
        prop_daily_detect::Array{Float64,2},
        index::Int64, sim_type::ContactSimulation)

For each policy defined in a contact quarantine simulation this function computes the mean and 95% quantiles for all statistics of interest. It operates in place on `global_metric_df` and `prop_daily_release_df`.

# Arguments
- `global_metric_df::DataFrame`: DataFrame which will contain the mean and 95% quantiles for statistics of interest from each policy run of a contact quarantine simulation. Entries are modified by this function.
- `prop_daily_detect_df::DataFrame`: DataFrame which will contain the mean and 95% quantiles for statistics of interest related to the proportion of quarantined contacts detected as cases, on each day of quarantine from each policy run of a contact quarantine simulation. Entries are modified by this function.
- `metric_df::DataFrame`: DataFrame containing point statistics from all iterations (in rows) for all statistics of interest (in columns) of a single policy run of a contact quarantine simulation.
- `prop_daily_detect::Array{Float64,2}`: An 2D array of the proportion of all individuals detected as cases on a given day, containing distinct simulation runs in columns and rows for each day (1 to `t_iso_end` for a given `policy`).
- `prop_daily_detect_of_detected::Array{Float64,2}`: An 2D array of the proportion of all detected individuals detected as cases on a given day, containing distinct simulation runs in columns and rows for each day (1 to `t_iso_end` for a given `policy`).
- `index::Int64`: The policy number the simulation is up to, corresponding to the equivalent row index in `global_metric_df` and used in conjunction with `max_iso_end_day` (computed within the function) to determine the relevant row indexes of `prop_daily_release_df`.
- `sim_type::ContactSimulation`: A `struct` specifying the simulation is for contact quarantine. Unused by the function other than to ensure that Julia's multiple dispatch calls this function rather the one for the case isolation simulation.

# Details
As mentioned in [`extractPropDailyMetrics`](@ref) policies can have different values of `t_iso_end` so we compute outputs for `prop_daily_release_df` based on the maximum `t_iso_end` of any policy, to make the computation and storage of global simulation metrics simpler.
"""
function computeGlobMetrics!(global_metric_df::DataFrame, prop_daily_detect_df::DataFrame,
    metric_df::DataFrame,
    prop_daily_detect::Array{Float64,2},
    prop_daily_detect_of_detected::Array{Float64,2},
    index::Int64, sim_type::ContactSimulation)

    quant_l = 0.025
    quant_u = 0.975

    # Global metrics ###########################################################
    # classifying columns
    # global_metric_df[index, :num_consec_tests] = numTestsToInt(policy.num_tests)
    # global_metric_df[index, :day_early_release] = policy.t_early_release * 1
    # global_metric_df[index, :day_iso_end] = policy.t_iso_end * 1

    # metric columns
    global_metric_df[index, :prop_ever_tested_positive_mean] = mean(metric_df.prop_ever_tested_positive)
    global_metric_df[index, :prop_ever_tested_positive_l] = quantile(metric_df.prop_ever_tested_positive, quant_l)
    global_metric_df[index, :prop_ever_tested_positive_u] = quantile(metric_df.prop_ever_tested_positive, quant_u)

    global_metric_df[index, :prop_ever_tested_positive_during_iso_mean] = mean(metric_df.prop_ever_tested_positive_during_iso)
    global_metric_df[index, :prop_ever_tested_positive_during_iso_l] = quantile(metric_df.prop_ever_tested_positive_during_iso, quant_l)
    global_metric_df[index, :prop_ever_tested_positive_during_iso_u] = quantile(metric_df.prop_ever_tested_positive_during_iso, quant_u)

    global_metric_df[index, :prop_ever_tested_positive_after_iso_mean] = mean(metric_df.prop_ever_tested_positive_after_iso)
    global_metric_df[index, :prop_ever_tested_positive_after_iso_l] = quantile(metric_df.prop_ever_tested_positive_after_iso, quant_l)
    global_metric_df[index, :prop_ever_tested_positive_after_iso_u] = quantile(metric_df.prop_ever_tested_positive_after_iso, quant_u)

    global_metric_df[index, :hours_infectious_in_community_mean] = mean(metric_df.hours_infectious_in_community_mean)
    global_metric_df[index, :hours_infectious_in_community_l] = quantile(metric_df.hours_infectious_in_community_mean, quant_l)
    global_metric_df[index, :hours_infectious_in_community_u] = quantile(metric_df.hours_infectious_in_community_mean, quant_u)

    # global_metric_df[index, :hours_not_infectious_in_community_mean] = mean(metric_df.hours_not_infectious_in_community_mean)
    # global_metric_df[index, :hours_not_infectious_in_community_l] = quantile(metric_df.hours_not_infectious_in_community_mean, quant_l)
    # global_metric_df[index, :hours_not_infectious_in_community_u] = quantile(metric_df.hours_not_infectious_in_community_mean, quant_u)

    global_metric_df[index, :num_tests_used_mean] = mean(metric_df.num_tests_used_mean)
    global_metric_df[index, :num_tests_used_l] = quantile(metric_df.num_tests_used_mean, quant_l)
    global_metric_df[index, :num_tests_used_u] = quantile(metric_df.num_tests_used_mean, quant_u)

    global_metric_df[index, :num_tests_used_during_iso_mean] = mean(metric_df.num_tests_used_during_iso_mean)
    global_metric_df[index, :num_tests_used_during_iso_l] = quantile(metric_df.num_tests_used_during_iso_mean, quant_l)
    global_metric_df[index, :num_tests_used_during_iso_u] = quantile(metric_df.num_tests_used_during_iso_mean, quant_u)

    global_metric_df[index, :num_tests_used_after_iso_mean] = mean(metric_df.num_tests_used_after_iso_mean)
    global_metric_df[index, :num_tests_used_after_iso_l] = quantile(metric_df.num_tests_used_after_iso_mean, quant_l)
    global_metric_df[index, :num_tests_used_after_iso_u] = quantile(metric_df.num_tests_used_after_iso_mean, quant_u)

    global_metric_df[index, :days_in_iso_because_sympt_not_quar_mean] = mean(metric_df.days_in_iso_because_sympt_not_quar_mean)
    global_metric_df[index, :days_in_iso_because_sympt_not_quar_l] = quantile(metric_df.days_in_iso_because_sympt_not_quar_mean, quant_l)
    global_metric_df[index, :days_in_iso_because_sympt_not_quar_u] = quantile(metric_df.days_in_iso_because_sympt_not_quar_mean, quant_u)

    global_metric_df[index, :days_in_iso_because_sympt_not_quar_during_iso_mean] = mean(metric_df.days_in_iso_because_sympt_not_quar_during_iso_mean)
    global_metric_df[index, :days_in_iso_because_sympt_not_quar_during_iso_l] = quantile(metric_df.days_in_iso_because_sympt_not_quar_during_iso_mean, quant_l)
    global_metric_df[index, :days_in_iso_because_sympt_not_quar_during_iso_u] = quantile(metric_df.days_in_iso_because_sympt_not_quar_during_iso_mean, quant_u)

    global_metric_df[index, :days_in_iso_because_sympt_not_quar_after_iso_mean] = mean(metric_df.days_in_iso_because_sympt_not_quar_after_iso_mean)
    global_metric_df[index, :days_in_iso_because_sympt_not_quar_after_iso_l] = quantile(metric_df.days_in_iso_because_sympt_not_quar_after_iso_mean, quant_l)
    global_metric_df[index, :days_in_iso_because_sympt_not_quar_after_iso_u] = quantile(metric_df.days_in_iso_because_sympt_not_quar_after_iso_mean, quant_u)

    global_metric_df[index, :days_in_iso_because_quar_not_sympt_mean] = mean(metric_df.days_in_iso_because_quar_not_sympt_mean)
    global_metric_df[index, :days_in_iso_because_quar_not_sympt_l] = quantile(metric_df.days_in_iso_because_quar_not_sympt_mean, quant_l)
    global_metric_df[index, :days_in_iso_because_quar_not_sympt_u] = quantile(metric_df.days_in_iso_because_quar_not_sympt_mean, quant_u)

    global_metric_df[index, :days_in_iso_because_quar_and_sympt_mean] = mean(metric_df.days_in_iso_because_quar_and_sympt_mean)
    global_metric_df[index, :days_in_iso_because_quar_and_sympt_l] = quantile(metric_df.days_in_iso_because_quar_and_sympt_mean, quant_l)
    global_metric_df[index, :days_in_iso_because_quar_and_sympt_u] = quantile(metric_df.days_in_iso_because_quar_and_sympt_mean, quant_u)


    # prop daily ###############################################################
    max_iso_end_day = maximum(prop_daily_detect_df.day)

    if minimum(prop_daily_detect_df.day) == 0
        max_iso_end_day+=1
    end

    end_index = max_iso_end_day * index
    start_index = end_index - max_iso_end_day + 1

    # proportion detected daily
    prop_metrics = extractPropDailyMetrics(prop_daily_detect, max_iso_end_day,
                                            quant_l, quant_u)

    # assign values
    prop_daily_detect_df[start_index:end_index, :prop_detected_mean] .= prop_metrics.prop_daily_mean
    prop_daily_detect_df[start_index:end_index, :prop_detected_l] .= prop_metrics.prop_daily_l
    prop_daily_detect_df[start_index:end_index, :prop_detected_u] .= prop_metrics.prop_daily_u

    # proportion detected daily of total detected
    prop_metrics = extractPropDailyMetrics(prop_daily_detect_of_detected, max_iso_end_day,
                                            quant_l, quant_u)

    # assign values
    prop_daily_detect_df[start_index:end_index, :prop_detected_of_detected_mean] .= prop_metrics.prop_daily_mean
    prop_daily_detect_df[start_index:end_index, :prop_detected_of_detected_l] .= prop_metrics.prop_daily_l
    prop_daily_detect_df[start_index:end_index, :prop_detected_of_detected_u] .= prop_metrics.prop_daily_u

    return nothing
end


"""
    writeMetrics!(global_metric_df::DataFrame, prop_daily_release_df::DataFrame,
        model::Union{IsolationModel, QuarantineModel, QuarantineModelExperimental}, num_sims::Int64, save_results::Bool=true,
        simulation_name::String="default", output_folder::String="outputs", num_decimals::Int64=4)

Writes simulation metrics stored in `global_metric_df` and `prop_daily_release_df` to `.csv` and model parameters used to `.txt` in the specified `output_folder` (must define a valid path but doesn't have to exist). Rounds any Float valued numbers in outputs to `num_decimals` decimal places (operates in place on `global_metric_df` and `prop_daily_release_df`).

# Arguments
- `global_metric_df::DataFrame`: DataFrame containing mean and 95% quantiles for statistics of interest from each policy run of a case isolation or contact quarantine simulation.
- `prop_daily_release_df::DataFrame`: DataFrame containing mean and 95% quantiles for statistics of interest related to the relevant proportions for a case isolation (see [`MitigatingIsolationAndQuarantine.computeGlobMetrics!`](@ref)).
- `model::Union{IsolationModel, QuarantineModel, QuarantineModelExperimental}`: `struct` containing all model parameter values (see [`MitigatingIsolationAndQuarantine.IsolationModel`](@ref) or [`MitigatingIsolationAndQuarantine.QuarantineModel`](@ref) or [`MitigatingIsolationAndQuarantine.QuarantineModelExperimental`](@ref)).
- `num_sims::Int64`: Number of simulation realisations per policy.
- `simulation_name::String`: The simulation name used to name output files. The value of `num_sims` will be appended to this `string` to produce distinct outputs in a given directory for different numbers of simulation realisations.
- `output_folder::String`: A valid path to a potential directory (either defined from the current working directory or as an absolute path). The directory does not have to exist - if it does not, the path to that directory and the directory itself will be created. Default is "default".
- `num_decimals::Int64`: The number of decimal places to be used in outputted simulation metrics. Default is 4 d.p.
"""
function writeMetrics!(global_metric_df::DataFrame, prop_daily_release_df::DataFrame,
    model::Union{IsolationModel, QuarantineModel, QuarantineModelExperimental}, num_sims::Int64,
    simulation_name::String="default", output_folder::String="outputs", num_decimals::Int64=4)

    file_name_scenario = simulation_name*"_"*string(num_sims)

    # make Float columns a fixed number of decimal places
    for i in 1:ncol(global_metric_df)
        if typeof(global_metric_df[1, i]) === Float64
            global_metric_df[:, i] .= round.(global_metric_df[:, i], digits=num_decimals)
        end
    end

    # make Float columns a fixed number of decimal places
    for i in 1:ncol(prop_daily_release_df)
        if typeof(prop_daily_release_df[1, i]) === Float64
            prop_daily_release_df[:, i] .= round.(prop_daily_release_df[:, i], digits=num_decimals)
        end
    end

    output_dir = joinpath(output_folder, simulation_name)

    if !isdir(output_dir)
        mkpath(output_dir)
    end

    # output csvs
    CSV.write(joinpath(output_dir, "isolation_metrics_"*file_name_scenario*".csv"), global_metric_df)

    if model isa IsolationModel
        CSV.write(joinpath(output_dir, "prop_released_"*file_name_scenario*".csv"), prop_daily_release_df)
    else
        CSV.write(joinpath(output_dir, "prop_detected_"*file_name_scenario*".csv"), prop_daily_release_df)
    end


    # output parameter csv
    open(joinpath(output_dir, "Parameters_"*file_name_scenario*".txt"), "w") do io
        println(io, "Global Model and Simulation Parameters")
        m = Pkg.Operations.Context().env.manifest
        println(io, "Package Version = ", string(m[findfirst(v->v.name == "MitigatingIsolationAndQuarantine", m)].version) )
        println(io, "Number of realisations per policy = ", num_sims)

        fieldnames_model = []

        if model isa IsolationModel
            println(io, "Model type = Case Isolation")
            fieldnames_model = fieldnames(IsolationModel)

            for (index, name) in enumerate(fieldnames_model)
                if index > 4
                    println(io, name, " = ", getproperty(model, name))
                end
            end
        elseif model isa QuarantineModel
            println(io, "Model type = Contact Isolation")

            fieldnames_model = fieldnames(QuarantineModel)

            for (index, name) in enumerate(fieldnames_model)
                if index > 5
                    println(io, name, " = ", getproperty(model, name))
                end
            end
        else 
            println(io, "Model type = Contact Isolation")

            fieldnames_model = fieldnames(QuarantineModelExperimental)

            for (index, name) in enumerate(fieldnames_model)
                if index > 6
                    println(io, name, " = ", getproperty(model, name))
                end
            end

        end
    end

    return nothing
end

"""
    warnOfMetricOverwrites(num_sims::Int64, output_folder::String, simulation_name::String,
        sim_type::Union{CaseSimulation, ContactSimulation})::Bool

Warns the user if any simulation outputs in the provided `output_folder` will be overwritten by the current simulation and provides a Y/N prompt asking whether the user wishes to continue. If the user chooses not to continue the function returns `false` and the simulation is terminated, otherwise it returns `true` and the simulation continues. Note: both a lower case or upper case 'Y' will continue the simulation.

# Arguments
- `num_sims::Int64`: Number of simulation realisations per policy.
- `output_folder::String`: A valid path to a potential directory (either defined from the current working directory or as an absolute path). The directory does not have to exist.
- `simulation_name::String`: The simulation name used to name output files. The value of `num_sims` will be appended to this `string` to produce distinct outputs in a given directory for different numbers of simulation realisations.
- `sim_type::Union{CaseSimulation, ContactSimulation}`:  A `struct` for whether the simulation is for case isolation [`MitigatingIsolationAndQuarantine.CaseSimulation`](@ref) or contact quarantine [`MitigatingIsolationAndQuarantine.ContactSimulation`](@ref).

!!! compat "VS Code and Julia REPL readline() bug" 
    If you are running simulations within Visual Studio Code there is a bug with how the function `readline()` reads information from the Julia REPL - per [julia-vscode-readline-issue](https://github.com/julia-vscode/julia-vscode/issues/785). No proper fixes exist as of end of 2022. The bug means that when a simulation is run in the Julia REPL, the prompt for whether to continue a simulation or not, will not read the first input given, but only the second input, which is not expected behaviour. 
    
    Workarounds are: 
    * If run in Julia REPL via VS Code, you need to enter the 'Y' (yes I wish to continue) twice. I.e. type in y once, hit enter, then y again and then enter. The first thing you type in is irrelevant as it will be ignored. To not continue, you type in any key, hit enter, then any key that isn't y and then enter.
    * Run simulations via the command line rather than within the Julia REPL in VS Code, e.g. using `julia --threads auto path_to_folder\\defaultContactSimulation.jl` on Windows or `julia --threads auto path_to_folder/defaultContactSimulation.jl` on MacOS/Linux. Then `readline()` works as expected; you only have to enter the 'Y'/'y' once.
"""
function warnOfMetricOverwrites(num_sims::Int64, output_folder::String, simulation_name::String,
    sim_type::Union{CaseSimulation, ContactSimulation})::Bool
    #=
    Warns if any metrics outputs in a given folder will be overwritten and provides a Y/N prompt,
    asking whether the user wishes to continue.
    =#

    output_dir = joinpath(output_folder, simulation_name)
    file_name_scenario = simulation_name*"_"*string(num_sims)

    if sim_type isa CaseSimulation
        possible_outputs = [joinpath(output_dir, "isolation_metrics_"*file_name_scenario*".csv"),
                            joinpath(output_dir, "prop_released_"*file_name_scenario*".csv"),
                            joinpath(output_dir, "Parameters_"*file_name_scenario*".txt"),
                            ]
    else
        possible_outputs = [joinpath(output_dir, "isolation_metrics_"*file_name_scenario*".csv"),
                            joinpath(output_dir, "prop_detected_"*file_name_scenario*".csv"),
                            joinpath(output_dir, "Parameters_"*file_name_scenario*".txt"),
                            ]
    end

    # check which outputs already exist
    output_exists = String[]

    for output in possible_outputs
        if isfile(output)
            push!(output_exists, output)
        end
    end

    if isempty(output_exists)
        return true
    else
        println()
        println("The following outputs will be overwritten:")
        println(output_exists)
        println("Do you wish to continue? (Y to continue)")
        input_val = readline()
        println()

        if lowercase(input_val) == "y"
            return true
        else
            return false
        end
    end
end
