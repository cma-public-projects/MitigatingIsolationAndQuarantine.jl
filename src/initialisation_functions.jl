#=
A file containing functions for the isolation datastructures
and variables represented in the simulation.

Author: Joel Trent
=#

"""
    numTestsToInt(num_tests::Union{NoTest, OneTest, TwoTests})::Number

Returns a numeric value for the number of consecutive negative tests required to release a case from it's `struct` representation.

# Arguments
- `num_tests::Union{NoTest, OneTest, TwoTests}`: The number of negative tests on consecutive days required to release a case early from isolation. For more see [`NoTest`](@ref), [`OneTest`](@ref) and [`TwoTests`](@ref).
"""
function numTestsToInt(num_tests::Union{NoTest, OneTest, TwoTests})::Number
    if num_tests isa NoTest
        return 0
    elseif num_tests isa OneTest
        return 1
    elseif num_tests isa TwoTests
        return 2
    end
    return NaN
end

"""
    calcMaxIso(policies::Union{Array{TestPolicy, 1}, 
                                Array{TestPolicyContact, 1}, 
                                Array{TestPolicyContactExperimental, 1} )::Number

Returns the maximum `t_iso_end` from all test policies using a generator to access all of the fields.

# Arguments
- `policies::Union{Array{TestPolicy, 1}, Array{TestPolicyContact, 1}, Array{TestPolicyContactExperimental, 1}`: An array of either [`TestPolicy`](@ref) or [`TestPolicyContact`](@ref) or [`TestPolicyContactExperimental`](@ref) `structs`.
"""
function calcMaxIso(policies::Union{Array{TestPolicy, 1}, Array{TestPolicyContact, 1}, Array{TestPolicyContactExperimental, 1} })::Number

    return maximum([policies[i].t_iso_end for i in 1:length(policies)])
end

"""
    initModelCase(pop_size::Int64, policy::TestPolicy,
        model_pars_struct::CaseDistributions)::IsolationModel

Returns a [`MitigatingIsolationAndQuarantine.IsolationModel`](@ref) `struct` which combines the provided population size with the fields of a [`TestPolicy`](@ref) and [`MitigatingIsolationAndQuarantine.CaseDistributions`](@ref) `struct`.

# Arguments
- `pop_size::Int64`: Size of simulated population of isolating cases.
- `policy::TestPolicy`: A valid [`TestPolicy`](@ref) `struct`.
- `model_pars_struct::CaseDistributions`: A [`MitigatingIsolationAndQuarantine.CaseDistributions`](@ref) `struct` containing model parameter distributions.
"""
function initModelCase(pop_size::Int64, policy::TestPolicy,
    model_pars_struct::CaseDistributions)::IsolationModel

    model = IsolationModel(0.0,
                            policy.t_early_release,
                            policy.t_iso_end,
                            policy.num_tests,
                            pop_size,
                            model_pars_struct.IPD_pars.IPD_shape_mean,
                            model_pars_struct.IPD_pars.IPD_shape_sd,
                            model_pars_struct.IPD_pars.IPD_scale_mean,
                            model_pars_struct.IPD_pars.IPD_scale_sd,
                            model_pars_struct.test_sensitivity_dist,
                            model_pars_struct.t_iso_entry_dist,
                            model_pars_struct.t_to_pos_dist,
                            model_pars_struct.t_to_neg_dist,
                            )

    return(model)
end

"""
    initModelContact(pop_size::Int64, policy::TestPolicyContact,
        model_pars_struct::ContactDistributions)::QuarantineModel

Returns a [`MitigatingIsolationAndQuarantine.QuarantineModel`](@ref) `struct` which combines the provided population size with the fields of a [`TestPolicyContact`](@ref) and [`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref) `struct`.

# Arguments
- `pop_size::Int64`: Size of simulated population of quarantined contacts.
- `policy::TestPolicyContact`: A valid [`TestPolicyContact`](@ref) `struct`.
- `model_pars_struct::ContactDistributions`: A [`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref) `struct` containing model parameter distributions.
"""
function initModelContact(pop_size::Int64, policy::TestPolicyContact,
    model_pars_struct::ContactDistributions)::QuarantineModel

    model = QuarantineModel(0.0,
                            policy.t_iso_end,
                            policy.symptomatic_test_policy,
                            policy.quarantine_policy,
                            policy.t_test_days,
                            pop_size,
                            model_pars_struct.IPD_pars.IPD_shape_mean,
                            model_pars_struct.IPD_pars.IPD_shape_sd,
                            model_pars_struct.IPD_pars.IPD_scale_mean,
                            model_pars_struct.IPD_pars.IPD_scale_sd,
                            model_pars_struct.test_sensitivity_dist,
                            model_pars_struct.test_sensitivity_asympt_dist,
                            model_pars_struct.t_iso_entry_dist,
                            model_pars_struct.t_to_infectious_dist,
                            model_pars_struct.t_to_pos_dist,
                            model_pars_struct.t_to_neg_dist,
                            model_pars_struct.t_sympt_start_dist,
                            model_pars_struct.t_sympt_end_dist,
                            model_pars_struct.prop_asympt_dist,
                            model_pars_struct.t_quarantine_start_dict
                            )

    return(model)
end

"""
    initModelContact(pop_size::Int64, policy::TestPolicyContactExperimental,
        model_pars_struct::ContactDistributions)::QuarantineModelExperimental

Returns a [`MitigatingIsolationAndQuarantine.QuarantineModelExperimental`](@ref) `struct` which combines the provided population size with the fields of a [`TestPolicyContactExperimental`](@ref) and [`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref) `struct`.

# Arguments
- `pop_size::Int64`: Size of simulated population of quarantined contacts.
- `policy::TestPolicyContactExperimental`: A valid [`TestPolicyContactExperimental`](@ref) `struct`.
- `model_pars_struct::ContactDistributions`: A [`MitigatingIsolationAndQuarantine.ContactDistributions`](@ref) `struct` containing model parameter distributions.
"""
function initModelContact(pop_size::Int64, policy::TestPolicyContactExperimental,
    model_pars_struct::ContactDistributions)::QuarantineModelExperimental

    model = QuarantineModelExperimental(0.0,
                            policy.t_iso_end,
                            policy.symptomatic_test_policy,
                            policy.quarantine_policy,
                            policy.t_test_days,
                            policy.prob_testing_on_test_days,
                            pop_size,
                            model_pars_struct.IPD_pars.IPD_shape_mean,
                            model_pars_struct.IPD_pars.IPD_shape_sd,
                            model_pars_struct.IPD_pars.IPD_scale_mean,
                            model_pars_struct.IPD_pars.IPD_scale_sd,
                            model_pars_struct.test_sensitivity_dist,
                            model_pars_struct.test_sensitivity_asympt_dist,
                            model_pars_struct.t_iso_entry_dist,
                            model_pars_struct.t_to_infectious_dist,
                            model_pars_struct.t_to_pos_dist,
                            model_pars_struct.t_to_neg_dist,
                            model_pars_struct.t_sympt_start_dist,
                            model_pars_struct.t_sympt_end_dist,
                            model_pars_struct.prop_asympt_dist,
                            model_pars_struct.t_quarantine_start_dict
                            )

    return(model)
end

"""
    initPopDataframe(model::IsolationModel)::DataFrame

Initialises and returns a DataFrame containing a simulated population of cases with attributes based on the provided model parameters. Each row represents a distinct case (individual). Also initialises zero valued columns used to track and calculate simulation statistics of interest.

# Arguments
- `model::IsolationModel`: [`MitigatingIsolationAndQuarantine.IsolationModel`](@ref) `struct` containing all policy and parameter information required to define each individual realisation of a singular policy within a case isolation simulation.

# Details
Initialises columns for each case's infectious period time, isolation entry time (and effective infectious end time), time a case can test positive from relative to the start of their infectious period and time a case no longer tests positive relative to the end of their infectious period are created, drawn from the provided parameter distributions on a per individual basis.

The following columns for each household contact that becomes a case are drawn from the provided parameter distributions in [`MitigatingIsolationAndQuarantine.IsolationModel`](@ref) independently for each case:
- `t_recovery`: Infectious period time (time taken to no longer be infectious). It is drawn from a Gamma distribution with parameters, `IPD_shape` and `IPD_scale`. `IPD_shape` and `IPD_scale` are random variables that are drawn once for each realisation from a Normal distribution, truncated between 0 and 100, with mean and standard deviation given by `model.IPD_shape_mean`, `model.IPD_shape_sd` and `model.IPD_scale_mean`, `model.IPD_scale_sd` respectively.
- `t_iso_entry_offset`: Time between the start of the infectious period and isolation entry time for a given case (isolation entry time for cases is always defined to occur exactly on day zero: ``t=0``). This is drawn from the distribution defined by `model.t_iso_entry_dist`.
- `t_infectious_end`: Infectious period end time. This is given by `t_recovery + t_iso_entry_offset`.
- `t_can_test_pos`: Time that a case can test positive from relative to `t_iso_entry_offset`. This is given by `t_iso_entry_offset + x`, where `x` is a random variable drawn from the distribution defined by `model.t_to_pos_dist`.
- `t_no_longer_test_pos`: Time that a case can no longer test positive from relative to `t_infectious_end`. This is given by `t_infectious_end + x`, where `x` is a random variable drawn from the distribution defined by `model.t_to_neg_dist`.

RAT test sensitivity for isolated cases is initialised in [`MitigatingIsolationAndQuarantine.caseIsolationSim`](@ref).
"""
function initPopDataframe(model::IsolationModel)::DataFrame

    population_df = DataFrame()

    # init columns
    population_df.id = collect(1:model.pop_size)

    # IPD shape and scale for the model is fixed per realisation
    population_df.t_recovery = rand.(
                                    Gamma(
                                    rand(truncated(Normal(model.IPD_shape_mean, model.IPD_shape_sd), 0, 100)),
                                    rand(truncated(Normal(model.IPD_scale_mean, model.IPD_scale_sd), 0, 100))
                                    ),
                                    model.pop_size)

    population_df.t_iso_entry_offset = rand(model.t_iso_entry_dist, model.pop_size)

    population_df.t_infectious_end = population_df.t_recovery .+ population_df.t_iso_entry_offset

    # time someone can test positive from
    population_df.t_can_test_pos = population_df.t_iso_entry_offset .+
                                        rand(model.t_to_pos_dist, model.pop_size)

    # time someone can no longer test positive from
    population_df.t_no_longer_test_pos = population_df.t_infectious_end .+
                                                rand(model.t_to_neg_dist, model.pop_size)

    population_df.released_and_infectious = BitArray(undef, model.pop_size) .* false
    population_df.released = BitArray(undef, model.pop_size) .* false
    population_df.t_released = zeros(Int64, model.pop_size)
    population_df.t_last_neg = zeros(Int64, model.pop_size)
    population_df.num_neg_consec = zeros(Int64, model.pop_size)

    population_df.t_excess_iso = zeros(model.pop_size)

    population_df.num_tests_used = zeros(Int64, model.pop_size)

    return population_df
end

"""
    initTestSensitivity(model::Union{QuarantineModel, QuarantineModelExperimental},
        population_df::DataFrame)::Array{Float64,1}

For given model parameter distributions for asymptomatic and symptomatic test sensitivity, returns a vector of test sensitivities for each individual in `population_df` drawn from the relevant distribution (based on whether they are symptomatic or asymptomatic).

# Arguments:
- `model::Union{QuarantineModel, QuarantineModelExperimental}`: [`MitigatingIsolationAndQuarantine.QuarantineModel`](@ref) or [`MitigatingIsolationAndQuarantine.QuarantineModelExperimental`](@ref) `struct` containing all policy and parameter information required to define each individual realisation of a singular policy within a contact quarantine simulation.
- `population_df::DataFrame`: A DataFrame containing the simulated population of household contacts with attributes based on the provided model parameters, generated by [`MitigatingIsolationAndQuarantine.initPopDataframeContact`](@ref).
"""
function initTestSensitivity(model::Union{QuarantineModel, QuarantineModelExperimental}, population_df::DataFrame)::Array{Float64,1}
    #=
    Initialise a test sensitivity for each individual dependent on whether
    they are symptomatic or not
    =#

    test_sensitivity = zeros(model.pop_size)

    asympt_inds = findall(.!population_df.is_symptomatic)
    test_sensitivity[asympt_inds] = rand(model.test_sensitivity_asympt_dist, length(asympt_inds))

    sympt_inds = findall(population_df.is_symptomatic)
    test_sensitivity[sympt_inds] = rand(model.test_sensitivity_dist, length(sympt_inds))

    return test_sensitivity
end

"""
    initPopDataframeContact(model::Union{QuarantineModel, QuarantineModelExperimental})::DataFrame

Initialises and returns a DataFrame containing a simulated population of household contacts with attributes based on the provided model parameters. Each row represents a distinct household contact (individual). Also initialises zero valued columns used to track and calculate simulation statistics of interest.

# Arguments
- `model::Union{QuarantineModel, QuarantineModelExperimental}`: [`MitigatingIsolationAndQuarantine.QuarantineModel`](@ref) or [`MitigatingIsolationAndQuarantine.QuarantineModelExperimental`](@ref) `struct` containing all policy and parameter information required to define each individual realisation of a singular policy within a contact quarantine simulation.

# Details
The following columns for each household contact that becomes a case are drawn from the provided parameter distributions in [`MitigatingIsolationAndQuarantine.QuarantineModel`](@ref) independently for each contact:
- `t_recovery`: Infectious period time (time taken to no longer be infectious). It is drawn from a Gamma distribution with parameters, `IPD_shape` and `IPD_scale`. `IPD_shape` and `IPD_scale` are random variables that are drawn once for each realisation from a Normal distribution, truncated between 0 and 100, with mean and standard deviation given by `model.IPD_shape_mean`, `model.IPD_shape_sd` and `model.IPD_scale_mean`, `model.IPD_scale_sd` respectively.
- `t_iso_entry_offset`: Time between the start of the infectious period and isolation entry time of the household case (isolation entry time for cases is always defined to occur exactly on day zero: ``t=0``), that transmitted the infection to this household contact. In the case where the time quarantine starts for a given contact, `population_df.t_quarantine_start`, is non-zero, this is the the time between the start of the infectious period and the day which is considered to be day zero for the household case, ``t=0``, that transmitted infection to this household contact. It is drawn from the distribution defined by `model.t_iso_entry_dist`.
- `t_infectious_start:` Infectious period start time of the household contact. This is given by `t_iso_entry_offset + x`, where `x` is a random variable drawn from the distribution defined by `model.t_to_infectious_dist`. This distribution for `x` represents the serial interval distribution for the time an infected contact would become infectious relative to the start of the household index case's infectious period.
- `t_infectious_end`: Infectious period end time of the household contact. This is given by `t_recovery + t_infectious_start`.
- `t_symptoms_start`: Time that symptoms start for the household contact relative to `t_infectious_start`. This is given by `t_infectious_start + x`, where `x` is a random variable drawn from the distribution defined by `model.t_sympt_start_dist`.
- `t_symptoms_end`: Time that symptoms end for the household contact relative to `t_infectious_end`. This is given by `t_infectious_end + x`, where `x` is a random variable drawn from the distribution defined by `model.t_sympt_end_dist`.
- `t_can_test_pos`: Time that a household contact can test positive from relative to `t_infectious_start`. This is given by `t_infectious_start + x`, where `x` is a random variable drawn from the distribution defined by `model.t_to_pos_dist`.
- `t_no_longer_test_pos`: Time that a household contact can no longer test positive from relative to `t_infectious_end`. This is given by `t_infectious_end + x`, where `x` is a random variable drawn from the distribution defined by `model.t_to_neg_dist`.
- `is_symptomatic`: Boolean for whether or not this household contact is symptomatic or asymptomatic. The proportion of contacts that are asymptomatic, `y`, is drawn from `model.prop_asympt_dist`. Then the first `round(y*model.pop_size)` rows of `population_df` are set to `false` (are asymptomatic) and the remainder are set to `true` (are symptomatic).
- `t_quarantine_start`: The time, in integer days, a quarantined contact begins quarantine relative to ``t=0``. Prior to `t_start_quarantine` being reached, this contact will not be tested or be required to quarantine. If `model.t_quarantine_start_dict["method"]` has type [`StartAtZero`](@ref), then this time will be ``t=0`` for all contacts. Otherwise, (when the type is either [`StartFitData`](@ref) or [`StartProvideTestDelay`](@ref)), this time is drawn as `model.t_quarantine_start_dict["rounding_func"].( max.(population_df.t_iso_entry_offset + rand(model.t_to_infectious_dist, model.pop_size) + x, x) )` where `x` is a random variable drawn from the distribution defined by `model.t_quarantine_start_dict["case_test_delay_dist"]`. This assumes that the quarantine start time of a contact is the day that the first case in the household is detected and enforces that symptom onset of that case (which is at ``t=0``) will occur before they test positive. I.e. the `max.` used sets the quarantine start time to be equal to `model.t_quarantine_start_dict["rounding_func"].(x)` when `population_df.t_iso_entry_offset + rand(model.t_to_infectious_dist, model.pop_size)` is less than zero to ensure that symptom onset of the first detected case occurs prior to it's detection (which is the case in the vast majority of NZ households). Discussion of how realistic some of the assumptions made for `t_quarantine_start` are and their consistency with other model assumptions is found in [`MitigatingIsolationAndQuarantine.testingDelayDF`](@ref).

RAT test sensitivity for quarantined contacts is initialised in [`MitigatingIsolationAndQuarantine.contactQuarantineSim`](@ref).
"""
function initPopDataframeContact(model::Union{QuarantineModel, QuarantineModelExperimental})::DataFrame

    population_df = DataFrame()

    # init columns
    population_df.id = collect(1:model.pop_size)

    # IPD shape and scale for the model is fixed per simulation
    population_df.t_recovery = rand.(
                                    Gamma(
                                    rand(truncated(Normal(model.IPD_shape_mean, model.IPD_shape_sd), 0, 100)),
                                    rand(truncated(Normal(model.IPD_scale_mean, model.IPD_scale_sd), 0, 100))
                                    ),
                                    model.pop_size)

    population_df.t_iso_entry_offset = rand(model.t_iso_entry_dist, model.pop_size)

    # time start quarantining relative to t=0
    if model.t_quarantine_start_dict["method"] isa StartAtZero
        population_df.t_quarantine_start = zeros(Int64, model.pop_size)
    else
        # population_df.t_quarantine_start = zeros(Int64, model.pop_size)

        t_case_can_test_pos = population_df.t_iso_entry_offset .+ rand(model.t_to_infectious_dist, model.pop_size)
        smallest_allowed_t = 0.0

        t_case_neg_bool = t_case_can_test_pos .< smallest_allowed_t
        not_t_case_neg_bool = .!t_case_neg_bool

        t_quarantine_start_continuous = zeros(model.pop_size)

        # quarantine start time for cases with negative time to being able to test positive, relative to symptom onset, t=0, is enforced to be at least zero.
        t_quarantine_start_continuous[t_case_neg_bool] .= rand(model.t_quarantine_start_dict["case_test_delay_dist"], sum(t_case_neg_bool))

        # Otherwise, quarantine start time is time infectious relative to symptom onset + time to being able to test positive +
        # some testing delay before testing positive.
        t_quarantine_start_continuous[not_t_case_neg_bool] .= t_case_can_test_pos[not_t_case_neg_bool] .+ rand(model.t_quarantine_start_dict["case_test_delay_dist"], sum(not_t_case_neg_bool))

        # discretise these quarantine start times using the provided rounding function
        population_df.t_quarantine_start = convert.(Int64, model.t_quarantine_start_dict["rounding_func"].(t_quarantine_start_continuous))
    end

    population_df.have_started_quarantine = BitArray(undef, model.pop_size) .* false

    # time infectious - the time to infectious must be between 0 and 14 days
    population_df.t_infectious_start = population_df.t_iso_entry_offset .+
                                    rand(model.t_to_infectious_dist, model.pop_size)

    # time symptoms start relative to infectious start
    population_df.t_symptoms_start = population_df.t_infectious_start .+
                                rand(model.t_sympt_start_dist, model.pop_size)

    # time someone is no longer infectious / they've recovered
    population_df.t_infectious_end = population_df.t_recovery .+ population_df.t_infectious_start

    population_df.t_symptoms_end = population_df.t_infectious_end .+
                                rand(model.t_sympt_end_dist, model.pop_size)


    # time someone can test positive from
    population_df.t_can_test_pos = population_df.t_infectious_start .+
                                     rand(model.t_to_pos_dist, model.pop_size)


    # time someone can no longer test positive from
    population_df.t_no_longer_test_pos = population_df.t_infectious_end .+
                                            rand(model.t_to_neg_dist, model.pop_size)

    # time someone tested positive
    population_df.t_tested_positive = zeros(Int64, model.pop_size)

    # we wouldn't test asymptomatic individuals in the same way as symptomatic (note
    # more efficient to have true be symptomatic)
    population_df.is_symptomatic = .!(BitArray(undef, model.pop_size) .* false)

    prop_asympt = rand(model.prop_asympt_dist)

    # set that proportion of rows to be asymptomatic (not symptomatic)
    population_df[1:convert(Int64, round(prop_asympt*nrow(population_df))), :is_symptomatic] .= false

    population_df.ever_tested_pos = BitArray(undef, model.pop_size) .* false

    population_df.t_in_community_infectious = zeros(model.pop_size)
    population_df.t_in_community = zeros(model.pop_size)

    population_df.t_in_iso_because_sympt_not_quar = zeros(Int64, model.pop_size)
    population_df.t_in_iso_because_quar_not_sympt = zeros(Int64, model.pop_size)
    population_df.t_in_iso_because_quar_and_sympt = zeros(Int64, model.pop_size)

    # population_df.t_released = zeros(Int64, model.pop_size)
    # population_df.released = BitArray(undef, model.pop_size) .* false

    # population_df.released_and_infectious = BitArray(undef, model.pop_size) .* false
    # population_df.t_released = zeros(Int64, model.pop_size)
    # population_df.t_last_neg = convert.(Int64, zeros(model.pop_size))
    # population_df.num_neg_consec = convert.(Int64, zeros(model.pop_size))
    #
    # population_df.t_excess_iso = zeros(model.pop_size)

    population_df.num_tests_used = zeros(Int64, model.pop_size)

    return population_df
end

"""
    initMetricDataframe(num_sims::Int64)::DataFrame

Initialises and returns a DataFrame for containing the metrics of interest (in columns) for all iterations (in rows) of a single policy in a case isolation simulation. Metrics of interest here refer to those that are not distributed over time which are covered by [`MitigatingIsolationAndQuarantine.initPropDailyDataframe`](@ref).
The value in every index of the returned DataFrame is zero with type `Float64`.

# Arguments
- `num_sims::Int64`: The number of distinct iterations of the simulation to run. `num_sims` is equal to the number of rows of the returned DataFrame.

Called by [`MitigatingIsolationAndQuarantine.casePolicySimulation`](@ref).
"""
function initMetricDataframe(num_sims::Int64)::DataFrame

    metric_df = DataFrame()

    # init columns
    metric_df.prop_released_infectious = zeros(num_sims)
    metric_df.t_infectious_after_release_mean = zeros(num_sims)
    metric_df.t_infectious_after_release_population_mean = zeros(num_sims)
    metric_df.t_excess_iso_per_person_mean = zeros(num_sims)
    metric_df.tests_used_per_case_mean = zeros(num_sims)
    metric_df.t_in_isolation_mean = zeros(num_sims)

    return metric_df
end

"""
    initMetricDataframeContact(num_sims::Int64)::DataFrame

Initialises and returns a DataFrame for containing the metrics of interest (in columns) for all iterations (in rows) of a single policy in a contact quarantine simulation. Metrics of interest here refer to those that are not distributed over time which are covered by [`MitigatingIsolationAndQuarantine.initPropDailyDetectDataframe`](@ref).
The value in every index of the returned DataFrame is zero with type `Float64`.

# Arguments
- `num_sims::Int64`: The number of distinct iterations of the simulation to run. `num_sims` is equal to the number of rows of the returned DataFrame.

Called by [`MitigatingIsolationAndQuarantine.contactPolicySimulation`](@ref).
"""
function initMetricDataframeContact(num_sims::Int64)::DataFrame

    metric_df = DataFrame()

    # init columns
    metric_df.prop_ever_tested_positive            = zeros(num_sims)
    metric_df.prop_ever_tested_positive_during_iso = zeros(num_sims)
    metric_df.prop_ever_tested_positive_after_iso  = zeros(num_sims)

    metric_df.hours_infectious_in_community_mean = zeros(num_sims)
    # metric_df.hours_not_infectious_in_community_mean = zeros(num_sims)
    metric_df.num_tests_used_mean = zeros(num_sims)
    metric_df.num_tests_used_during_iso_mean = zeros(num_sims)
    metric_df.num_tests_used_after_iso_mean = zeros(num_sims)

    metric_df.days_in_iso_because_sympt_not_quar_mean            = zeros(num_sims)
    metric_df.days_in_iso_because_sympt_not_quar_during_iso_mean = zeros(num_sims)
    metric_df.days_in_iso_because_sympt_not_quar_after_iso_mean  = zeros(num_sims)

    metric_df.days_in_iso_because_quar_not_sympt_mean = zeros(num_sims)
    metric_df.days_in_iso_because_quar_and_sympt_mean = zeros(num_sims)

    return metric_df
end

"""
    initPolicyDataframe(policies::Array{TestPolicy, 1})::DataFrame

Initialises and returns a DataFrame which converts the information contained within each [`TestPolicy`](@ref) `struct` in `policies` into a row of a DataFrame is used to save the information of what policy was used with a corresponding set of metrics.

# Arguments
- `policies::Array{TestPolicy, 1}`: A 1D array/vector of [`TestPolicy`](@ref) `structs` to be run by a case isolation simulation. `length(policies)` is equal to the number of rows of the returned DataFrame.

Called by [`MitigatingIsolationAndQuarantine.initGlobMetricDataframe`](@ref) and [`MitigatingIsolationAndQuarantine.initPropDailyDataframe`](@ref).
"""
function initPolicyDataframe(policies::Array{TestPolicy, 1})::DataFrame

    num_policies = length(policies)

    policy_df = DataFrame()
    policy_df.policy_number = collect(1:num_policies)
    policy_df.num_consec_tests = [numTestsToInt(policies[i].num_tests) for i in 1:num_policies]
    policy_df.day_early_release = [policies[i].t_early_release for i in 1:num_policies]
    policy_df.day_iso_end = [policies[i].t_iso_end for i in 1:num_policies]

    return policy_df
end

"""
    initPolicyDataframeContact(policies::Array{TestPolicyContact, 1})::DataFrame

Initialises and returns a DataFrame which converts the information contained within each [`TestPolicyContact`](@ref) `struct` in `policies` into a row of a DataFrame is used to save the information of what policy was used with a corresponding set of metrics.

# Arguments
- `policies::Array{TestPolicyContact, 1}`: A 1D array/vector of [`TestPolicyContact`](@ref) `structs` to be run by a contact quarantine simulation. `length(policies)` is equal to the number of rows of the returned DataFrame.

Called by [`MitigatingIsolationAndQuarantine.initGlobMetricDataframeContact`](@ref) and [`MitigatingIsolationAndQuarantine.initPropDailyDetectDataframe`](@ref).
"""
function initPolicyDataframeContact(policies::Array{TestPolicyContact, 1})::DataFrame

    num_policies = length(policies)

    policy_df = DataFrame()
    policy_df.policy_number = collect(1:num_policies)
    policy_df.day_iso_end = [policies[i].t_iso_end for i in 1:num_policies]
    policy_df.symptomatic_test_policy = [policies[i].symptomatic_test_policy for i in 1:num_policies]
    policy_df.quarantine_policy = [policies[i].quarantine_policy for i in 1:num_policies]
    policy_df.test_days = [policies[i].t_test_days for i in 1:num_policies]

    return policy_df
end

"""
    initPolicyDataframeContact(policies::Array{TestPolicyContactExperimental, 1})::DataFrame

Initialises and returns a DataFrame which converts the information contained within each [`TestPolicyContactExperimental`](@ref) `struct` in `policies` into a row of a DataFrame is used to save the information of what policy was used with a corresponding set of metrics.

# Arguments
- `policies::Array{TestPolicyContactExperimental, 1}`: A 1D array/vector of [`TestPolicyContactExperimental`](@ref) `structs` to be run by a contact quarantine simulation. `length(policies)` is equal to the number of rows of the returned DataFrame.

Called by [`MitigatingIsolationAndQuarantine.initGlobMetricDataframeContact`](@ref) and [`MitigatingIsolationAndQuarantine.initPropDailyDetectDataframe`](@ref).
"""
function initPolicyDataframeContact(policies::Array{TestPolicyContactExperimental, 1})::DataFrame

    num_policies = length(policies)

    policy_df = DataFrame()
    policy_df.policy_number = collect(1:num_policies)
    policy_df.day_iso_end = [policies[i].t_iso_end for i in 1:num_policies]
    policy_df.symptomatic_test_policy = [policies[i].symptomatic_test_policy for i in 1:num_policies]
    policy_df.quarantine_policy = [policies[i].quarantine_policy for i in 1:num_policies]
    policy_df.test_days = [policies[i].t_test_days for i in 1:num_policies]
    policy_df.prob_testing_on_test_days = [policies[i].prob_testing_on_test_days for i in 1:num_policies]

    return policy_df
end

"""
    initGlobMetricDataframe(policies::Array{TestPolicy, 1})::DataFrame

Initialises and returns a DataFrame for containing the overall (global) metrics of interest (in columns) for all policies (in rows) in a case isolation simulation.

# Arguments
- `policies::Array{TestPolicy, 1}`: A 1D array/vector of [`TestPolicy`](@ref) `structs` to be run by a case isolation simulation. `length(policies)` is equal to the number of rows of the returned DataFrame.

# Details
Metrics of interest here refer to those that are not distributed over time which are covered by [`MitigatingIsolationAndQuarantine.initPropDailyDataframe`](@ref). The DataFrame will contain the mean and 95% quantiles for these metrics of interest from each policy run.

Using the policy number, the index of a given policy in the `policies` array, the DataFrame of policy information returned by [`MitigatingIsolationAndQuarantine.initPolicyDataframe`](@ref) is left joined to the global metric DataFrame so that the statistics in each row are matched to the policy that created them.

Called by [`MitigatingIsolationAndQuarantine.casePolicySimulation`](@ref).
"""
function initGlobMetricDataframe(policies::Array{TestPolicy, 1})::DataFrame

    num_policies = length(policies)
    global_metric_df = DataFrame()

    # init columns
    # classifying columns
    global_metric_df.policy_number = collect(1:num_policies)

    # policy columns to leftjoin
    policy_df = initPolicyDataframe(policies)

    # add policy number columns to global_metric_df
    leftjoin!(global_metric_df, policy_df, on=:policy_number)

    # global_metric_df.num_consec_tests = Array{Int64}(undef, num_policies) .* 0
    # global_metric_df.day_early_release = Array{Int64}(undef, num_policies) .* 0
    # global_metric_df.day_iso_end = Array{Int64}(undef, num_policies) .* 0

    # metric columns
    global_metric_df.prop_released_infectious_mean = zeros(num_policies)
    global_metric_df.prop_released_infectious_l = zeros(num_policies)
    global_metric_df.prop_released_infectious_u = zeros(num_policies)

    global_metric_df.hours_infectious_after_release_mean = zeros(num_policies)
    global_metric_df.hours_infectious_after_release_l = zeros(num_policies)
    global_metric_df.hours_infectious_after_release_u = zeros(num_policies)

    global_metric_df.hours_infectious_after_release_population_mean = zeros(num_policies)
    global_metric_df.hours_infectious_after_release_population_l = zeros(num_policies)
    global_metric_df.hours_infectious_after_release_population_u = zeros(num_policies)

    global_metric_df.hours_excess_iso_per_person_mean = zeros(num_policies)
    global_metric_df.hours_excess_iso_per_person_l = zeros(num_policies)
    global_metric_df.hours_excess_iso_per_person_u = zeros(num_policies)

    global_metric_df.tests_used_per_case_mean = zeros(num_policies)
    global_metric_df.tests_used_per_case_l = zeros(num_policies)
    global_metric_df.tests_used_per_case_u = zeros(num_policies)

    global_metric_df.days_in_isolation_mean = zeros(num_policies)
    global_metric_df.days_in_isolation_l = zeros(num_policies)
    global_metric_df.days_in_isolation_u = zeros(num_policies)

    return global_metric_df
end

"""
    initGlobMetricDataframeContact(policies::Union{Array{TestPolicyContact, 1}, Array{TestPolicyContactExperimental, 1}})::DataFrame

Initialises and returns a DataFrame for containing the overall (global) metrics of interest (in columns) for all policies (in rows) in a contact quarantine simulation.

# Arguments
- `policies::Union{Array{TestPolicyContact, 1}, Array{TestPolicyContactExperimental, 1}}`: A 1D array/vector of [`TestPolicyContact`](@ref) `structs` to be run by a contact quarantine simulation. `length(policies)` is equal to the number of rows of the returned DataFrame.

# Details
Metrics of interest here refer to those that are not distributed over time which are covered by [`MitigatingIsolationAndQuarantine.initPropDailyDataframe`](@ref).
The DataFrame will contain the mean and 95% quantiles for these metrics of interest from each policy run.

Using the policy number, the index of a given policy in the `policies` array, the DataFrame of policy information returned by [`MitigatingIsolationAndQuarantine.initPolicyDataframeContact`](@ref) is left joined to the global metric DataFrame so that the statistics in each row are matched to the policy that created them.

Called by [`MitigatingIsolationAndQuarantine.contactPolicySimulation`](@ref).
"""
function initGlobMetricDataframeContact(policies::Union{Array{TestPolicyContact, 1}, Array{TestPolicyContactExperimental, 1}})::DataFrame

    num_policies = length(policies)
    global_metric_df = DataFrame()

    # init columns
    # classifying columns
    global_metric_df.policy_number = collect(1:num_policies)

    # policy columns to leftjoin
    policy_df = initPolicyDataframeContact(policies)

    # add policy number columns to global_metric_df
    leftjoin!(global_metric_df, policy_df, on=:policy_number)


    # global_metric_df.num_consec_tests = Array{Int64}(undef, num_policies) .* 0
    # # global_metric_df.day_early_release = Array{Int64}(undef, num_policies) .* 0
    # global_metric_df.day_iso_end = Array{Int64}(undef, num_policies) .* 0
    # global_metric_df.symptomatic_test_policy = Array{Union{TestDailyNo, TestDailyYes}}(undef, num_policies)
    # global_metric_df.quarantine_policy = Array{Union{NoQuarantine, QuarantineSymptoms, QuarantineIsoEndDays}}(undef, num_policies)
    # global_metric_df.test_days = Array{Array{Int64,1},1}(undef, num_policies)

    # metric columns
    global_metric_df.prop_ever_tested_positive_mean = zeros(num_policies)
    global_metric_df.prop_ever_tested_positive_l = zeros(num_policies)
    global_metric_df.prop_ever_tested_positive_u = zeros(num_policies)

    global_metric_df.prop_ever_tested_positive_during_iso_mean = zeros(num_policies)
    global_metric_df.prop_ever_tested_positive_during_iso_l = zeros(num_policies)
    global_metric_df.prop_ever_tested_positive_during_iso_u = zeros(num_policies)

    global_metric_df.prop_ever_tested_positive_after_iso_mean = zeros(num_policies)
    global_metric_df.prop_ever_tested_positive_after_iso_l = zeros(num_policies)
    global_metric_df.prop_ever_tested_positive_after_iso_u = zeros(num_policies)

    global_metric_df.hours_infectious_in_community_mean = zeros(num_policies)
    global_metric_df.hours_infectious_in_community_l = zeros(num_policies)
    global_metric_df.hours_infectious_in_community_u = zeros(num_policies)

    # global_metric_df.hours_not_infectious_in_community_mean = zeros(num_policies)
    # global_metric_df.hours_not_infectious_in_community_l = zeros(num_policies)
    # global_metric_df.hours_not_infectious_in_community_u = zeros(num_policies)

    global_metric_df.num_tests_used_mean = zeros(num_policies)
    global_metric_df.num_tests_used_l = zeros(num_policies)
    global_metric_df.num_tests_used_u = zeros(num_policies)

    global_metric_df.num_tests_used_during_iso_mean = zeros(num_policies)
    global_metric_df.num_tests_used_during_iso_l = zeros(num_policies)
    global_metric_df.num_tests_used_during_iso_u = zeros(num_policies)

    global_metric_df.num_tests_used_after_iso_mean = zeros(num_policies)
    global_metric_df.num_tests_used_after_iso_l = zeros(num_policies)
    global_metric_df.num_tests_used_after_iso_u = zeros(num_policies)

    global_metric_df.days_in_iso_because_sympt_not_quar_mean = zeros(num_policies)
    global_metric_df.days_in_iso_because_sympt_not_quar_l = zeros(num_policies)
    global_metric_df.days_in_iso_because_sympt_not_quar_u = zeros(num_policies)

    global_metric_df.days_in_iso_because_sympt_not_quar_during_iso_mean = zeros(num_policies)
    global_metric_df.days_in_iso_because_sympt_not_quar_during_iso_l = zeros(num_policies)
    global_metric_df.days_in_iso_because_sympt_not_quar_during_iso_u = zeros(num_policies)

    global_metric_df.days_in_iso_because_sympt_not_quar_after_iso_mean = zeros(num_policies)
    global_metric_df.days_in_iso_because_sympt_not_quar_after_iso_l = zeros(num_policies)
    global_metric_df.days_in_iso_because_sympt_not_quar_after_iso_u = zeros(num_policies)

    global_metric_df.days_in_iso_because_quar_not_sympt_mean = zeros(num_policies)
    global_metric_df.days_in_iso_because_quar_not_sympt_l = zeros(num_policies)
    global_metric_df.days_in_iso_because_quar_not_sympt_u = zeros(num_policies)

    global_metric_df.days_in_iso_because_quar_and_sympt_mean = zeros(num_policies)
    global_metric_df.days_in_iso_because_quar_and_sympt_l = zeros(num_policies)
    global_metric_df.days_in_iso_because_quar_and_sympt_u = zeros(num_policies)

    return global_metric_df
end

"""
    initPropDailyDataframe(policies::Array{TestPolicy, 1})::DataFrame

Initialises and returns a DataFrame for containing the overall (global) daily metrics of interest (in columns) for all policies (in rows) in a case isolation simulation.

# Arguments
- `policies::Array{TestPolicy, 1}`: A 1D array/vector of [`TestPolicy`](@ref) `structs` to be run by a case isolation simulation. `length(policies) * max_iso_end_day`, where `max_iso_end_day = calcMaxIso(policies)` is equal to the number of rows of the returned DataFrame.

# Details
The DataFrame will contain the mean and 95% quantiles for these daily metrics of interest from each policy run. Because policies can have different values of `t_iso_end` the number of rows (days) sequentially recorded for each policy is based on the maximum `t_iso_end` of any policy, to make the computation and storage of these metrics simpler.
Days of interest for these metrics start at day 1.

Using the policy number, the index of a given policy in the `policies` array, the DataFrame of policy information returned by [`MitigatingIsolationAndQuarantine.initPolicyDataframe`](@ref) is left joined to the daily metric DataFrame so that the statistics in each row are matched to the policy that created them.

Called by [`MitigatingIsolationAndQuarantine.casePolicySimulation`](@ref).
"""
function initPropDailyDataframe(policies::Array{TestPolicy, 1})::DataFrame

    max_iso_end_day = calcMaxIso(policies)
    num_policies = length(policies)

    prop_daily_release_df = DataFrame()

    prop_daily_release_df.policy_number = [i for i in 1:num_policies for j in 1:max_iso_end_day]

    # policy columns to leftjoin
    policy_df = initPolicyDataframe(policies)

    # add policy number columns to prop_daily_release_df
    leftjoin!(prop_daily_release_df, policy_df, on=:policy_number)

    prop_daily_release_df.day = [j for i in 1:num_policies for j in 1:max_iso_end_day]
    prop_daily_release_df.prop_released_mean = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_released_l = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_released_u = zeros(num_policies*max_iso_end_day)

    prop_daily_release_df.prop_released_infectious_mean = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_released_infectious_l = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_released_infectious_u = zeros(num_policies*max_iso_end_day)

    prop_daily_release_df.prop_released_not_infectious_mean = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_released_not_infectious_l = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_released_not_infectious_u = zeros(num_policies*max_iso_end_day)

    prop_daily_release_df.prop_not_released_infectious_mean = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_not_released_infectious_l = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_not_released_infectious_u = zeros(num_policies*max_iso_end_day)

    prop_daily_release_df.prop_not_released_not_infectious_mean = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_not_released_not_infectious_l = zeros(num_policies*max_iso_end_day)
    prop_daily_release_df.prop_not_released_not_infectious_u = zeros(num_policies*max_iso_end_day)

    return prop_daily_release_df
end

"""
    initPropDailyDetectDataframe(policies::Union{Array{TestPolicyContact, 1}, Array{TestPolicyContactExperimental, 1}})::DataFrame

Initialises and returns a DataFrame for containing the overall (global) daily metrics of interest (in columns) for all policies (in rows) in a contact quarantine simulation.

# Arguments
- `policies::Union{Array{TestPolicyContact, 1}, Array{TestPolicyContactExperimental, 1}}`: A 1D array/vector of [`TestPolicyContact`](@ref) `structs` to be run by a contact quarantine simulation. `length(policies) * (max_iso_end_day+8)`, where `max_iso_end_day = calcMaxIso(policies)` is equal to the number of rows of the returned DataFrame. It is `+8` because we start recording from day 0 (rather than 1) and we consider 7 additional days after `t_iso_end`.

# Details
The DataFrame will contain the mean and 95% quantiles for these daily metrics of interest from each policy run. Because policies can have different values of `t_iso_end` the number of rows (days) sequentially recorded for each policy is based on the maximum `t_iso_end` of any policy, to make the computation and storage of these metrics simpler.
Moreover, it is possible for contacts to become cases after `t_iso_end`, hence each simulation simulates for 7 additional days after `t_iso_end`.
Days of interest for these metrics start at day 0.

Using the policy number, the index of a given policy in the `policies` array, the DataFrame of policy information returned by [`MitigatingIsolationAndQuarantine.initPolicyDataframeContact`](@ref) is left joined to the daily metric DataFrame so that the statistics in each row are matched to the policy that created them.

Called by [`MitigatingIsolationAndQuarantine.contactPolicySimulation`](@ref).
"""
function initPropDailyDetectDataframe(policies::Union{Array{TestPolicyContact, 1}, Array{TestPolicyContactExperimental, 1}})::DataFrame

    max_iso_end_day = calcMaxIso(policies)
    num_policies = length(policies)

    prop_daily_release_df = DataFrame()

    prop_daily_release_df.policy_number = [i for i in 1:num_policies for j in 0:(max_iso_end_day+7)]

    # policy columns to leftjoin
    policy_df = initPolicyDataframeContact(policies)

    # add policy number columns to prop_daily_release_df
    leftjoin!(prop_daily_release_df, policy_df, on=:policy_number)

    prop_daily_release_df.day = [j for i in 1:num_policies for j in 0:(max_iso_end_day+7)]
    prop_daily_release_df.prop_detected_mean = zeros(num_policies*(max_iso_end_day+8))
    prop_daily_release_df.prop_detected_l = zeros(num_policies*(max_iso_end_day+8))
    prop_daily_release_df.prop_detected_u = zeros(num_policies*(max_iso_end_day+8))
    prop_daily_release_df.prop_detected_of_detected_mean = zeros(num_policies*(max_iso_end_day+8))
    prop_daily_release_df.prop_detected_of_detected_l = zeros(num_policies*(max_iso_end_day+8))
    prop_daily_release_df.prop_detected_of_detected_u = zeros(num_policies*(max_iso_end_day+8))

    return prop_daily_release_df
end
